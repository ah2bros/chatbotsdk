package com.gemvietnam.chatapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vtt.chatlib.ChatBotUtils;
import com.vtt.chatlib.utils.*;
import com.vtt.chatlib.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
  public static final int OVERLAY_PERMISSION_REQ_CODE_CHATHEAD = 96;
  public static final int VOICE_RECOGNITION_REQUEST = 104;
  public static ChatBotUtils.OnChatBotActionDone onChatBotActionDone;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    findViewById(R.id.txt_hello).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        connectChatHead(MainActivity.this);
      }
    });

  }

  public static void connectChatHead(final Activity activity) {

    if (!com.vtt.chatlib.utils.Utils.canDrawOverlays(activity)) {
      requestPermission(OVERLAY_PERMISSION_REQ_CODE_CHATHEAD, activity);
      return;
    }

    ChatBotUtils.connectChatHead(activity, new DataSend(), "mbi", null);

    ChatBotUtils.getInstance().setOnChatActionCallback(new ChatBotUtils.OnChatActionCallback() {
      @Override
      public void onAction(String json, final ChatBotUtils.OnChatBotActionDone onChatBotActionDone) {


      }

      @Override
      public void onVoiceRequest(ChatBotUtils.OnChatBotActionDone onChatBotActionDonee) {
        onVoiceRequestMess(activity);
        onChatBotActionDone = onChatBotActionDonee;
      }

      @Override
      public void onErrorMessage(Response response) {
        Log.d("Log", "error");
      }
    });

  }

  public static void onVoiceRequestMess(Activity activity) {
    try {
      Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
      intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
          RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
      intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
          "Please speak slowly and enunciate clearly.");
      activity.startActivityForResult(intent, VOICE_RECOGNITION_REQUEST);
    } catch (Exception ex) {
      Logger.log(ex);
      Toast.makeText(activity, "Thiết bị không hỗ trợ hoặc đang tắt tính năng này", Toast.LENGTH_LONG).show();
    }

  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == OVERLAY_PERMISSION_REQ_CODE_CHATHEAD) {
      if (!Utils.canDrawOverlays(MainActivity.this)) {

      } else {
        connectChatHead(this);
      }

    } else if (requestCode == VOICE_RECOGNITION_REQUEST && resultCode == RESULT_OK) {
      ArrayList matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
      final String firstMatch = matches.get(0).toString();

      new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
          if (onChatBotActionDone != null) {
            onChatBotActionDone.onVoiceRecognitionResult(firstMatch);
          }
        }
      }, 1000);
    }
  }

  public static void requestPermission(int requestCode, Activity activity) {
    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
    intent.setData(Uri.parse("package:" + activity.getPackageName()));
    activity.startActivityForResult(intent, requestCode);
  }

  static class DataSend {
    private String username = "Tran Hieu";
    private String deptLevel = "1";
    private String token = "Qc/W/uVkFHKpPR1JmFVUj23s31QD5/Ir54+F2wD0i44WUqFJB7Q8mPjvlp+tkMLK4DvyQneNcq/G0jltfqMO6A==";
    private String device = "ANDROID";

    public DataSend() {
    }
  }
}
