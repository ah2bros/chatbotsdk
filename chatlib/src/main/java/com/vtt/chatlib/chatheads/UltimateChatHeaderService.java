package com.vtt.chatlib.chatheads;

/**
 * Created by phamh_000 on 05/12/2017.
 */

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.flipkart.circularImageView.CircularDrawable;
import com.flipkart.circularImageView.notification.CircularNotificationDrawer;
import com.vtt.chatlib.R;
import com.vtt.chatlib.chatdetail.ChatBot;
import com.vtt.chatlib.chatheads.ui.ChatHead;
import com.vtt.chatlib.chatheads.ui.ChatHeadViewAdapter;
import com.vtt.chatlib.chatheads.ui.MaximizedArrangement;
import com.vtt.chatlib.chatheads.ui.MinimizedArrangement;
import com.vtt.chatlib.chatheads.ui.container.DefaultChatHeadManager;
import com.vtt.chatlib.chatheads.ui.container.WindowManagerContainer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class UltimateChatHeaderService extends Service {

  // Binder given to clients
  private final IBinder mBinder = new LocalBinder();
  private DefaultChatHeadManager<String> chatHeadManager;
  private int chatHeadIdentifier = 0;
  private WindowManagerContainer windowManagerContainer;
  private Map<String, View> viewCache = new HashMap<>();

  public int onStartCommand(Intent intent, int flags, int startId) {
    return START_NOT_STICKY;
  }

  @Override
  public IBinder onBind(Intent intent) {
    return mBinder;
  }

  @Override
  public void onCreate() {
    super.onCreate();

    windowManagerContainer = new WindowManagerContainer(this);
    chatHeadManager = new DefaultChatHeadManager<String>(this, windowManagerContainer);
    chatHeadManager.setViewAdapter(new ChatHeadViewAdapter<String>() {

      @Override
      public View attachView(String key, ChatHead chatHead, ViewGroup parent) {
        View cachedView = viewCache.get(key);
//        if (cachedView == null) {
//          LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
//          View view = inflater.inflate(R.layout.fragment_test, parent, false);
//          TextView identifier = (TextView) view.findViewById(R.id.identifier);
//          identifier.setText(key);
//          cachedView = view;
//          viewCache.put(key, view);
//        }
//        parent.addView(cachedView);
        return cachedView;
      }

      @Override
      public void detachView(String key, ChatHead<? extends Serializable> chatHead, ViewGroup parent) {
        View cachedView = viewCache.get(key);
        if (cachedView != null) {
          parent.removeView(cachedView);
        }
      }

      @Override
      public void removeView(String key, ChatHead<? extends Serializable> chatHead, ViewGroup parent) {
        View cachedView = viewCache.get(key);
        if (cachedView != null) {
          viewCache.remove(key);
          parent.removeView(cachedView);
        }
      }

      @Override
      public Drawable getChatHeadDrawable(String key) {
        return UltimateChatHeaderService.this.getChatHeadDrawable(key);
      }
    });

    addChatHead();

    chatHeadManager.setArrangement(MinimizedArrangement.class, null);
//    moveToForeground();

  }

  private Drawable getChatHeadDrawable(String key) {
//    Random rnd = new Random();
//    int randomColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    CircularDrawable circularDrawable = new CircularDrawable();
    Drawable d = getResources().getDrawable(R.drawable.ic_einstein_chat_head);
    Drawable currentState = d.getCurrent();

    if (currentState instanceof BitmapDrawable) {
      circularDrawable.setBitmapOrTextOrIcon(((BitmapDrawable) currentState).getBitmap());
    }

//    circularDrawable.setBitmapOrTextOrIcon(new TextDrawer().setText("C" + key).setBackgroundColor(randomColor));
//    int badgeCount = (int) (Math.random() * 10f);
//    circularDrawable.setNotificationDrawer(new CircularNotificationDrawer().setNotificationText(String.valueOf(badgeCount)).setNotificationAngle(135).setNotificationColor(Color.WHITE, Color.RED));
    circularDrawable.setBorder(Color.WHITE, 3);
    return circularDrawable;

  }

  private void moveToForeground() {
    Notification notification = new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.notification_template_icon_bg)
        .setContentTitle("Springy heads")
        .setContentText("Click to configure.")
        .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, ChatBot.class), 0))
        .build();

    startForeground(1, notification);
  }

  public void addChatHead() {
//    chatHeadIdentifier++;
    chatHeadManager.addChatHead(String.valueOf(chatHeadIdentifier), false, true);
    chatHeadManager.bringToFront(chatHeadManager.findChatHeadByKey(String.valueOf(chatHeadIdentifier)));
  }

  public void removeChatHead() {
    chatHeadManager.removeChatHead(String.valueOf(chatHeadIdentifier), true);
    chatHeadIdentifier--;
  }

  public void removeAllChatHeads() {
    chatHeadIdentifier = 0;
    chatHeadManager.removeAllChatHeads(true);
  }

  public void toggleArrangement() {
    if (chatHeadManager.getActiveArrangement() instanceof MinimizedArrangement) {
      chatHeadManager.setArrangement(MaximizedArrangement.class, null);
    } else {
      chatHeadManager.setArrangement(MinimizedArrangement.class, null);
    }
  }

  public void updateBadgeCount() {
    chatHeadManager.reloadDrawable(String.valueOf(chatHeadIdentifier));
  }

  public void showLoading() {
    if (chatHeadManager.getArrowLayout() != null && chatHeadManager.getArrowLayout().getChatHeadContentView() != null)
      chatHeadManager.getArrowLayout().getChatHeadContentView().addLoadingMessage();
  }

  public void hideLoading() {
    if (chatHeadManager.getArrowLayout() != null && chatHeadManager.getArrowLayout().getChatHeadContentView() != null)
      chatHeadManager.getArrowLayout().getChatHeadContentView().removeLoadingMessage();

  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    windowManagerContainer.destroy();
  }

  public void minimize() {
    chatHeadManager.setArrangement(MinimizedArrangement.class, null);
  }

  public void maximize() {
    chatHeadManager.setArrangement(MaximizedArrangement.class, null);
  }

  /**
   * Class used for the client Binder.  Because we know this service always
   * runs in the same process as its clients, we don't need to deal with IPC.
   */
  public class LocalBinder extends Binder {
    public UltimateChatHeaderService getService() {
      // Return this instance of LocalService so clients can call public methods
      return UltimateChatHeaderService.this;
    }
  }

  @Override
  public void onTaskRemoved(Intent rootIntent) {
    //Code here
    stopSelf();
  }
}
