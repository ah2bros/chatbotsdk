package com.vtt.chatlib.chatheads;

import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vtt.chatlib.ChatBotUtils;
import com.vtt.chatlib.R;
import com.vtt.chatlib.chatdetail.data.MessagesFixtures;
import com.vtt.chatlib.chatdetail.data.model.AvailableFeature;
import com.vtt.chatlib.chatdetail.data.model.ConclusionMessage;
import com.vtt.chatlib.chatdetail.data.model.Message;
import com.vtt.chatlib.chatkit.commons.ImageLoader;
import com.vtt.chatlib.chatkit.messages.AvailableFeatureAdapter;
import com.vtt.chatlib.chatkit.messages.MessageHolders;
import com.vtt.chatlib.chatkit.messages.MessageInput;
import com.vtt.chatlib.chatkit.messages.MessagesList;
import com.vtt.chatlib.chatkit.messages.MessagesListAdapter;
import com.vtt.chatlib.chatkit.models.Button;
import com.vtt.chatlib.chatkit.models.QuickReply;
import com.vtt.chatlib.mbi.constant.MBIUtils;
import com.vtt.chatlib.mbi.constant.ServiceType;
import com.vtt.chatlib.mbi.dto.blockInfo.InfoBlock;
import com.vtt.chatlib.mbi.dto.horizontalChart.ItemHorizontalStackRankingUnitData;
import com.vtt.chatlib.mbi.dto.lineChart.ItemLineChartServiceDTO;
import com.vtt.chatlib.model.ChatBotRequest;
import com.vtt.chatlib.model.ChatBotResponse;
import com.vtt.chatlib.model.MessageRequest;
import com.vtt.chatlib.model.SenderRequest;
import com.vtt.chatlib.network.WSRestful;
import com.vtt.chatlib.utils.Constants;
import com.vtt.chatlib.utils.EasyDialog;
import com.vtt.chatlib.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vtt.chatlib.chatdetail.data.MessagesFixtures.getBotUser;
import static com.vtt.chatlib.utils.Constants.TYPE_ACTION;
import static com.vtt.chatlib.utils.Constants.TYPE_IMAGE;
import static com.vtt.chatlib.utils.Constants.TYPE_LINK;
import static com.vtt.chatlib.utils.Constants.TYPE_QUICK_ACTION_DISLIKE;
import static com.vtt.chatlib.utils.Constants.TYPE_QUICK_ACTION_LIKE;
import static com.vtt.chatlib.utils.Constants.TYPE_QUICK_ACTION_TEXT;

/**
 * Created by phamh_000 on 06/12/2017.
 */

public class ChatHeadContentView extends FrameLayout
    implements MessageInput.InputListener,
    MessageInput.AttachmentsListener, MessagesListAdapter.SelectionListener,
    MessagesListAdapter.OnLoadMoreListener, MessagesListAdapter.OnMessageActionClickListener,
    MessagesListAdapter.OnMessageButtonClickListener, ChatBotUtils.OnChatBotActionDone,
    MessagesListAdapter.OnMessageViewLongClickListener<Message>, AvailableFeatureAdapter.OnItemClick, MessageHolders.OnItemViewClick {

  static ChatHeadContentView chatHeadContentView;

  public static synchronized ChatHeadContentView getInstance(Context context) {
    if (chatHeadContentView == null)
      chatHeadContentView = new ChatHeadContentView(context);
    return chatHeadContentView;
  }

  private ChatHeadContentView(@NonNull Context context) {
    super(context);
    init();
  }

  private static final int TOTAL_MESSAGES_COUNT = 100;
  private MessagesList messagesList;
  MessageInput input;
  private TextView txtWarning;
  protected ImageLoader imageLoader;
  private int selectionCount;
  private Date lastLoadedDate;
  private Menu menu;
  private ImageView btnCopy;
  protected MessagesListAdapter<Message> messagesAdapter;
  protected static final String senderId = "0";
  static final int MIN_KEYBOARD_HEIGHT_PX = 150;

  private Object currentContext = null;

  private void init() {
    inflate(getContext(), R.layout.service_chat_head, this);

    new WSRestful(getContext()).resetServiceMBI();

    imageLoader = new ImageLoader() {
      @Override
      public void loadImage(ImageView imageView, String url) {
        com.vtt.chatlib.image.ImageLoader.setImage(getContext(), url, imageView);
      }
    };

    this.messagesList = (MessagesList) findViewById(R.id.messagesList);
    txtWarning = (TextView) findViewById(R.id.txt_warning);
    initAdapter();

    input = (MessageInput) findViewById(R.id.input);
    input.setInputListener(this);

    btnCopy = (ImageView) findViewById(R.id.btnCopy);
    btnCopy.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        messagesAdapter.copySelectedMessagesText(getContext(), getMessageStringFormatter(), true);
        Toast.makeText(getContext(), R.string.copied_message, Toast.LENGTH_LONG).show();
      }
    });

    loadData(ChatBotUtils.DOMAIN, Constants.TYPE_TEXT, "Hi", null);

    this.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
      private final Rect windowVisibleDisplayFrame = new Rect();
      private int lastVisibleDecorViewHeight;

      @Override
      public void onGlobalLayout() {
        // Retrieve visible rectangle inside window.
        ChatHeadContentView.this.getWindowVisibleDisplayFrame(windowVisibleDisplayFrame);
        final int visibleDecorViewHeight = windowVisibleDisplayFrame.height();

        // Decide whether keyboard is visible from changing decor view height.
        if (lastVisibleDecorViewHeight != 0) {
          if (lastVisibleDecorViewHeight > visibleDecorViewHeight + MIN_KEYBOARD_HEIGHT_PX) {
            messagesAdapter.getLayoutManager().scrollToPosition(0);
          }
        }

        if (lastVisibleDecorViewHeight != 0)
          if (lastVisibleDecorViewHeight > visibleDecorViewHeight) Utils.isKeyBoardShow = true;
          else if (lastVisibleDecorViewHeight < visibleDecorViewHeight)
            Utils.isKeyBoardShow = false;

        lastVisibleDecorViewHeight = visibleDecorViewHeight;
      }
    });

  }

  public void addLoadingMessage() {
    messagesAdapter.addToStart(MessagesFixtures.getImageLoadingMessage(), true);
  }

  public void removeLoadingMessage() {
    messagesAdapter.removeBotLoading();
  }

  @Override
  public boolean onSubmit(CharSequence text) {
    Message message = new Message(MessagesFixtures.getRandomId(), MessagesFixtures.getMyUser(), text.toString());
    removeLoadingMessage();
    messagesAdapter.addToStart(message, true);

    addLoadingMessage();

    //Goi API de lay cau tra loi
    loadData(ChatBotUtils.DOMAIN, Constants.TYPE_TEXT, input.getText(), null);

    return true;
  }

  private void gotoAnswer(String type) {
    if (type.equals(Constants.TYPE_TEXT)) {
      Message message = new Message(MessagesFixtures.getRandomId(), MessagesFixtures.getBotUser(), "Xin chào! Chúc bạn một ngày vui vẻ!");
      messagesAdapter.addToStart(message, true);
    } else if (type.equals(TYPE_IMAGE)) {
      messagesAdapter.addToStart(MessagesFixtures.getImageMessage(), true);
    } else if (type.equals(TYPE_LINK)) {
      messagesAdapter.addToStart(MessagesFixtures.getLinkMessage(), true);
    } else if (type.equals(TYPE_ACTION)) {
      messagesAdapter.addToStart(MessagesFixtures.getActionMessage(), true);
    }
  }

  @Override
  public boolean onVoiceClick() {
    if (ChatBotUtils.getInstance().getOnChatActionCallback() != null) {
      ChatBotUtils.minimize();
      ChatBotUtils.getInstance().getOnChatActionCallback().onVoiceRequest(this);
    }
    return true;
  }

  @Override
  public void onAddAttachments() {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        messagesAdapter.addToStart(MessagesFixtures.getImageMessage(), true);
      }
    });
  }

  private void initAdapter() {

    initNewAdapter();
//        messagesAdapter = new MessagesListAdapter<>(senderId, holdersConfig, imageLoader);

//    messagesAdapter.enableSelectionMode(this);
    messagesAdapter.setLoadMoreListener(this);
    messagesAdapter.setOnMessageActionClickListener(this);
    messagesAdapter.setOnAvailableFeatureClickListener(this);
    messagesAdapter.setOnConclusionMessageClick(this);
    messagesAdapter.setOnMessageButtonClickListener(this);
    messagesAdapter.setOnMessageViewLongClickListener(this);

    messagesAdapter.registerViewClickListener(R.id.messageUserAvatar,
        new MessagesListAdapter.OnMessageViewClickListener<Message>() {
          @Override
          public void onMessageViewClick(View view, Message message) {
//                        AppUtils.showToast(ChatBot.this,
//                                message.getUser().getName() + " avatar click",
//                                false);
          }
        });
    this.messagesList.setAdapter(messagesAdapter);
  }

  public void initNewAdapter() {
    messagesAdapter = new MessagesListAdapter<>(getContext(), senderId, imageLoader);
  }

  @Override
  public void onLoadMore(int page, int totalItemsCount) {
    if (totalItemsCount < TOTAL_MESSAGES_COUNT) {
//            loadMessloadMessages();
    }
  }

  protected void loadMessages() {
    new Handler().postDelayed(new Runnable() { //imitation of internet connection
      @Override
      public void run() {
        ArrayList<Message> messages = MessagesFixtures.getMessages(lastLoadedDate);
        lastLoadedDate = messages.get(messages.size() - 1).getCreatedAt();
        messagesAdapter.addToEnd(messages, false);
      }
    }, 1000);
  }

  @Override
  public void onSelectionChanged(int count) {
    this.selectionCount = count;
    if (count > 0)
      btnCopy.setVisibility(View.VISIBLE);
    else
      btnCopy.setVisibility(View.GONE);
  }

  public void loadData(String domain, String type, String text, Object payload) {
    WSRestful restful = new WSRestful(getContext());
    SenderRequest sender = new SenderRequest(domain, ChatBotUtils.data);
    long time = System.currentTimeMillis();
    MessageRequest message = new MessageRequest(text, type, time + "", payload);

    restful.chatbotMessage(ChatBotUtils.headers, new ChatBotRequest(sender, message, currentContext),
        new Callback<ChatBotResponse>() {
          @Override
          public void onResponse(Call<ChatBotResponse> call, final Response<ChatBotResponse> response) {
            showWarningConnected();
            if (response != null && response.isSuccessful()) {
              handleSuccessfulResponse(response);
            } else {
              executeErrorResponse(response);
            }

            removeLoadingMessage();
          }

          @Override
          public void onFailure(Call<ChatBotResponse> call, Throwable t) {
            removeLoadingMessage();
            showErrorConnection();
          }
        });
  }

  public void showErrorConnection() {
    txtWarning.setText(getResources().getString(R.string.not_connected));
    txtWarning.setBackgroundDrawable(getResources().getDrawable(R.drawable.warning_not_connected_bg));
    txtWarning.setVisibility(VISIBLE);
  }

  public void showWarningConnected() {
    if (txtWarning.getVisibility() == VISIBLE) {
      txtWarning.setText(getResources().getString(R.string.connected));
      txtWarning.setBackgroundDrawable(getResources().getDrawable(R.drawable.warning_connneted_bg));
      new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
          txtWarning.setVisibility(GONE);
        }
      }, 1500);
    }
  }

  private void handleSuccessfulResponse(final Response<ChatBotResponse> response) {
    if (response.body() != null && response.body().getResponse() != null) {
      currentContext = response.body().getResponse().getContext();

      if (response.body().getResponse().getChart() != null) {
        Message message = null;
        if (MBIUtils.getItemViewType(response.body().getResponse().getChart()) == ServiceType.SERVICE_REPORT.ordinal()) {
          message = new Message(MessagesFixtures.getRandomId(), getBotUser(), null);
          message.setBlockInfo((InfoBlock) response.body().getResponse().getChart().getData());
        } else if (MBIUtils.getItemViewType(response.body().getResponse().getChart()) == ServiceType.RANKING_INFO.ordinal()) {
          message = new Message(MessagesFixtures.getRandomId(), getBotUser(), null);
          message.setRankingInfo((InfoBlock) response.body().getResponse().getChart().getData());
        } else if (MBIUtils.getItemViewType(response.body().getResponse().getChart()) == ServiceType.LINE_CHART.ordinal()) {
          message = new Message(MessagesFixtures.getRandomId(), getBotUser(), null);
          message.setItemLineChart((ItemLineChartServiceDTO) response.body().getResponse().getChart().getData());
        } else if (MBIUtils.getItemViewType(response.body().getResponse().getChart()) == ServiceType.HORIZONTAL_STACK_RANKING_UNIT.ordinal()) {
          message = new Message(MessagesFixtures.getRandomId(), getBotUser(), null);
          message.setItemHorizontalChart((ItemHorizontalStackRankingUnitData) response.body().getResponse().getChart().getData());
        }

        if (message != null) {
          message.setPayload(response.body().getResponse().getPayload());
          messagesAdapter.addToStart(message, true);
        }
        new Handler().postDelayed(new Runnable() {
          @Override
          public void run() {
            messagesAdapter.getLayoutManager().scrollToPosition(0);
          }
        }, 100);

      } else if (response.body().getType().equals(Constants.TYPE_METADATA)) {
//        loadDataMBI(response.body().getResponse().getParameters());

      } else if (response.body().getType().equals(Constants.TYPE_ACTION)) {
        addSpeechMessage(response.body());

        final Message message = new Message(MessagesFixtures.getRandomId(), getBotUser(), null);
        Message.QuickReply quickReplies = response.body().getResponse().getMessageAction();
        message.setMessageAction(quickReplies);
        if (quickReplies != null && quickReplies.getActionList() != null && quickReplies.getActionList().size() > 0) {
          runOnUiThread(new Runnable() {
            @Override
            public void run() {
              messagesAdapter.addToStart(message, true);
            }
          });
        }

      } else if (response.body().getType().equals(Constants.TYPE_TEXT)) {
        String type = response.body().getType();
        if (type.equals(Constants.TYPE_TEXT)) {
          Message message = new Message(MessagesFixtures.getRandomId(), getBotUser(), response.body().getResponse().getSpeech());
          messagesAdapter.addToStart(message, true);
        }
      } else if (response.body().getType().equals(Constants.TYPE_ACTION_CARD)) {
        addSpeechMessage(response.body());

        final Message message = new Message(MessagesFixtures.getRandomId(), getBotUser(), null);
        ArrayList<AvailableFeature> availableFeature = response.body().getResponse().getAvailableFeatures();
        message.setAvailableFeatures(availableFeature);
        if (availableFeature != null && availableFeature.size() > 0) {
          runOnUiThread(new Runnable() {
            @Override
            public void run() {
              messagesAdapter.addToStart(message, true);
            }
          });
        }
      } else if (response.body().getType().equals(Constants.TYPE_ACTION_REDIRECT)) {
        addSpeechMessage(response.body());

        final Message message = new Message(MessagesFixtures.getRandomId(), getBotUser(), null);
        ConclusionMessage conclusion = response.body().getResponse().getConclusionMessage();
        message.setConclusion(conclusion);
        if (conclusion != null) {
          runOnUiThread(new Runnable() {
            @Override
            public void run() {
              messagesAdapter.addToStart(message, true);
            }
          });
        }
      }

    } else {
      executeErrorResponse(response);
    }
  }

  private void addSpeechMessage(final ChatBotResponse chatBotResponse) {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        if (chatBotResponse.getResponse().getSpeech() != null && !chatBotResponse.getResponse().getSpeech().isEmpty()) {
          final Message message1 =
              new Message(MessagesFixtures.getRandomId(), getBotUser(), chatBotResponse.getResponse().getSpeech());
          messagesAdapter.addToStart(message1, true);
        }
      }
    });

  }

  private void runOnUiThread(Runnable runnable) {
    Handler handler = new Handler(Looper.getMainLooper());
    handler.post(runnable);
  }

  public void executeErrorResponse(Response<ChatBotResponse> response) {
    new WSRestful(getContext()).resetServiceAll();
    if (ChatBotUtils.getInstance().getOnChatActionCallback() != null)
      ChatBotUtils.getInstance().getOnChatActionCallback().onErrorMessage(response);
  }

  public void loadDataMBI(final ChatBotResponse.Parameters request) {
    WSRestful restful = new WSRestful(getContext());
    restful.mBIMessage(null, request, new Callback<ChatBotResponse>() {
      @Override
      public void onResponse(Call<ChatBotResponse> call, Response<ChatBotResponse> response) {
        if (response != null && response.body() != null) {
          String type = response.body().getType();
          if (type.equals(Constants.TYPE_TEXT)) {
            Message message = new Message(MessagesFixtures.getRandomId(), getBotUser(), response.body().getResponse().getSpeech());
            messagesAdapter.addToStart(message, true);
          }
        }

        messagesAdapter.removeBotLoading();

      }

      @Override
      public void onFailure(Call<ChatBotResponse> call, Throwable t) {
        messagesAdapter.removeBotLoading();
      }
    });
  }

  @Override
  public void onMessageQuickReplyClick(QuickReply message, int holderPosition) {
    if (holderPosition != -1) {
      messagesAdapter.getListItem().remove(holderPosition);
      messagesAdapter.notifyItemRemoved(holderPosition);
    }

    //Quick reply
    if (message.getDest() != null && message.getDest().trim().equals(Constants.KEY_ACTION_SERVER)) {
      if (message.getContentType().trim().toLowerCase().equals(TYPE_QUICK_ACTION_TEXT)) {
        final Message messageRequest = new Message(MessagesFixtures.getRandomId(), MessagesFixtures.getMyUser()
            , message.getTitle());
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            messagesAdapter.addToStart(messageRequest, true);
          }
        });

      } else if (message.getContentType().trim().toLowerCase().equals(TYPE_QUICK_ACTION_LIKE)) {
        final Message messagee = new Message(MessagesFixtures.getRandomId(), MessagesFixtures.getMyUser(), null);
        if (message.getTitle().trim().toLowerCase().equals(TYPE_QUICK_ACTION_LIKE)) {
          messagee.setImage(new Message.Image(R.drawable.ic_like_message + ""));

        } else if (message.getTitle().trim().toLowerCase().equals(TYPE_QUICK_ACTION_DISLIKE)) {
          messagee.setImage(new Message.Image(R.drawable.ic_dislike_message + ""));
        }
        if (messagee.getImageUrl() != null)
          runOnUiThread(new TimerTask() {
            @Override
            public void run() {
              messagesAdapter.addToStart(messagee, true);
            }
          });
      }

      loadData(ChatBotUtils.DOMAIN, Constants.TYPE_ACTION, null, message.getPayload());

    } else if (message.getDest() != null && message.getDest().trim().equals(Constants.KEY_ACTION_APP)) {
      if (ChatBotUtils.getInstance().getOnChatActionCallback() != null) {
        ChatBotUtils.getInstance().getOnChatActionCallback()
            .onAction(null == message.getPayload() ? "" : new Gson().toJson(message.getPayload()), this);
      }
    }

  }

  @Override
  public void onItemAvailableFeatureClick(AvailableFeature availableFeature) {
    final Message messageRequest = new Message(MessagesFixtures.getRandomId(), MessagesFixtures.getMyUser()
        , availableFeature.getTitle());
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        messagesAdapter.addToStart(messageRequest, true);
      }
    });

    loadData(ChatBotUtils.DOMAIN, Constants.TYPE_ACTION, null, availableFeature.getPayload());
  }

  @Override
  public void onItemConclusionClick(ConclusionMessage conclusionMessage) {
    if (ChatBotUtils.getInstance().getOnChatActionCallback() != null) {
      ChatBotUtils.getInstance().getOnChatActionCallback()
          .onAction(null == conclusionMessage.getPayload() ? "" : new Gson().toJson(conclusionMessage.getPayload()), this);
    }
  }

  @Override
  public void onItemChartClick(Object payload) {
    if (ChatBotUtils.getInstance().getOnChatActionCallback() != null) {
      ChatBotUtils.getInstance().getOnChatActionCallback()
          .onAction(null == payload ? "" : new Gson().toJson(payload), this);
    }
  }

  @Override
  public void onMessageClick(Button message) {
    final Message messageRequest = new Message(MessagesFixtures.getRandomId(), MessagesFixtures.getMyUser(), message.getTitle());
//        messagesAdapter.addToStart(messageRequest, true);
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        messagesAdapter.addToStart(messageRequest, true);
      }
    });
  }


  private MessagesListAdapter.Formatter<Message> getMessageStringFormatter() {
    return new MessagesListAdapter.Formatter<Message>() {
      @Override
      public String format(Message message) {
//                String createdAt = new SimpleDateFormat("MMM d, EEE 'at' h:mm a", Locale.getDefault())
//                        .format(message.getCreatedAt());
//
//                String text = message.getText();
//                if (text == null) text = "[attachment]";
//
//                return String.format(Locale.getDefault(), "%s: %s (%s)",
//                        message.getUser().getName(), text, createdAt);

        return message.getText();
      }
    };
  }

  @Override
  public void onActionDone(String json) {
    ChatBotUtils.maximize();
    Gson gson = new Gson();
    ChatBotResponse response = gson.fromJson(json, ChatBotResponse.class);
    removeLoadingMessage();
    if (response != null && response.getResponse() != null && response.getResponse().getSpeech() != null) {
      String type = response.getType();
      if (type.equals(Constants.TYPE_TEXT)) {
        Message message = new Message(MessagesFixtures.getRandomId(), getBotUser(), response.getResponse().getSpeech());
        messagesAdapter.addToStart(message, true);
      }
    }
  }

  @Override
  public void onVoiceRecognitionResult(String firstMatch) {
    Message message = new Message(MessagesFixtures.getRandomId(), MessagesFixtures.getMyUser(), firstMatch);
    messagesAdapter.addToStart(message, true);
    loadData(Constants.DOMAIN, Constants.TYPE_TEXT, firstMatch, null);
  }

  @Override
  public void onMessageViewLongClick(final View view, final Message message) {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        View rateView = LayoutInflater.from(getContext()).inflate(R.layout.view_rate_message, null);
        rateView.findViewById(R.id.txt_copy).setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
            Utils.copyToClipboard(getContext(), message.getText());
            easyDialog.dismiss();
          }
        });
        View anchor = view.findViewById(R.id.bubble);
        if (anchor == null) anchor = view.findViewById(R.id.image_message);
        if (anchor != null) {
          showEasyDialogNoTriagle(getContext(), rateView, anchor);
        } else
          showEasyDialogNoTriagle(getContext(), rateView, view);
      }
    }, 100);
  }

  static EasyDialog easyDialog;

  public static synchronized void showEasyDialogNoTriagle(final Context context, View layout, View anchorView) {
    easyDialog = new EasyDialog(context).setLayout(layout)
        .setBackgroundColor(ContextCompat.getColor(context, android.R.color.white))
        .setLocationByAttachedView(anchorView)
        .setGravity(EasyDialog.GRAVITY_TOP)
        .setTouchOutsideDismiss(true)
        .setMatchParent(false)
        .setOutsideColor(ContextCompat.getColor(context, R.color.easy_out_side_color))
        .setMarginLeftAndRight(0, 0).showAlert(Utils.isKeyBoardShow);
  }

}
