package com.vtt.chatlib.chatdetail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vtt.chatlib.R;
import com.vtt.chatlib.chatdetail.data.MessagesFixtures;
import com.vtt.chatlib.chatdetail.data.model.Message;
import com.vtt.chatlib.chatkit.commons.ImageLoader;
import com.vtt.chatlib.chatkit.messages.MessageInput;
import com.vtt.chatlib.chatkit.messages.MessagesList;
import com.vtt.chatlib.chatkit.messages.MessagesListAdapter;
import com.vtt.chatlib.chatkit.models.Button;
import com.vtt.chatlib.chatkit.models.QuickReply;
import com.vtt.chatlib.model.ChatBotRequest;
import com.vtt.chatlib.model.ChatBotResponse;
import com.vtt.chatlib.model.MessageRequest;
import com.vtt.chatlib.model.SenderRequest;
import com.vtt.chatlib.network.WSRestful;
import com.vtt.chatlib.network.socket.WebSocketRTCClient;
import com.vtt.chatlib.utils.Constants;
import com.vtt.chatlib.utils.Logger;
import com.vtt.chatlib.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vtt.chatlib.chatdetail.data.MessagesFixtures.getBotUser;
import static com.vtt.chatlib.utils.Constants.TYPE_ACTION;
import static com.vtt.chatlib.utils.Constants.TYPE_IMAGE;
import static com.vtt.chatlib.utils.Constants.TYPE_LINK;

public class ChatBot extends AppCompatActivity
    implements MessageInput.InputListener,
    MessageInput.AttachmentsListener, MessagesListAdapter.SelectionListener,
    MessagesListAdapter.OnLoadMoreListener, MessagesListAdapter.OnMessageActionClickListener, MessagesListAdapter.OnMessageButtonClickListener, MessagesListAdapter.OnMessageViewLongClickListener<Message> {

  WebSocketRTCClient appRtcClient;
  static String appName;
  static String account;
  static String accessToken;
  static String msisdn;
  static String token;
  static String mid;

  public static final String HEADER = "header";
  public static final String TOOLBAR_TITLE = "toolbar_title";
//  public static final String HOST = "host";
//  public static String ENDPOINT_CHATBOT = "http://10.60.5.245:8057/vBIServices/";
//  public static String ENDPOINT_MBI = "http://10.60.129.68:8057/vBIServices/";
  protected HashMap<String, String> headers = new HashMap<>();

  public static void open(Context context, HashMap<String, String> headers, String host, String toolbarTitle) {
//    ENDPOINT_CHATBOT = host;
//    ENDPOINT_MBI = host;
    Intent intent = new Intent(context, ChatBot.class);
    intent.putExtra(HEADER, headers);
    intent.putExtra(TOOLBAR_TITLE, toolbarTitle);
    context.startActivity(intent);
  }

  public static void open(Context context, String input) {
    //token, user_type, service_type, contract_id, cusld

    appName = "MYVIETTEL";
    account = "myviettel";
    accessToken = "myviettel@123";
    msisdn = "0969113369";
    token = Utils.generateToken();
    mid = "mid.1457764197618:41d102a3e1ae206a38";

    context.startActivity(new Intent(context, ChatBot.class));
  }

  private static final int VOICE_RECOGNITION_REQUEST = 100;
  static ChatBot chatDetailActivity;

  public static ChatBot getInstance() {
    if (chatDetailActivity == null)
      chatDetailActivity = new ChatBot();
    return chatDetailActivity;
  }

  private static final int TOTAL_MESSAGES_COUNT = 100;
  private MessagesList messagesList;
  MessageInput input;
  protected ImageLoader imageLoader;
  private int selectionCount;
  private Date lastLoadedDate;
  private Menu menu;
  private ImageView btnCopy;
  protected MessagesListAdapter<Message> messagesAdapter;
  protected static final String senderId = "0";
  boolean active = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(R.layout.activity_chat_detail);
    new WSRestful(this).resetServiceMBI();

    imageLoader = new ImageLoader() {
      @Override
      public void loadImage(ImageView imageView, String url) {
        com.vtt.chatlib.image.ImageLoader.setImage(ChatBot.this, url, imageView);
      }
    };

    this.messagesList = (MessagesList) findViewById(R.id.messagesList);
    initAdapter();

    input = (MessageInput) findViewById(R.id.input);
    input.setInputListener(this);

    headers = (HashMap<String, String>) getIntent().getSerializableExtra(HEADER);

    String toolbarTitle = getIntent().getStringExtra(TOOLBAR_TITLE);
    if (!TextUtils.isEmpty(toolbarTitle)) {
      ((TextView) findViewById(R.id.tvTitle)).setText(toolbarTitle);
    }

    ImageView btnBack = (ImageView) findViewById(R.id.btnBack);
    btnBack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        hideKeBoard();
        onBackPressed();
      }
    });

    btnCopy = (ImageView) findViewById(R.id.btnCopy);
    btnCopy.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        messagesAdapter.copySelectedMessagesText(ChatBot.this, getMessageStringFormatter(), true);
        Toast.makeText(ChatBot.this, R.string.copied_message, Toast.LENGTH_LONG).show();
      }
    });
//        initSocket();
    initAnotate();

    loadData(Constants.DOMAIN, Constants.TYPE_TEXT, "Hi");
  }

  public void initAnotate() {
  }

  @Override
  public boolean onSubmit(CharSequence text) {
    Message message = new Message(MessagesFixtures.getRandomId(), MessagesFixtures.getMyUser(), text.toString());
    messagesAdapter.removeBotLoading();
    messagesAdapter.addToStart(message, true);

    messagesAdapter.addToStart(MessagesFixtures.getImageLoadingMessage(), true);

    //Goi API de lay cau tra loi
//        if(text.toString().toLowerCase().contains("Doanh thu".toLowerCase())){
//            gotoAnswer(TYPE_IMAGE);
//        }
//        else
//         if(text.toString().toLowerCase().contains("Danh sách".toLowerCase()) || text.toString().toLowerCase().contains("Danh sach".toLowerCase())){
//             gotoAnswer(TYPE_ACTION);
//        }
//        else if(text.toString().toLowerCase().contains("Link".toLowerCase())){
//            gotoAnswer(TYPE_LINK);
//        }
//        else if(text.toString().toLowerCase().contains("area".toLowerCase()) || text.toString().toLowerCase().contains("Bieu Do".toLowerCase())){
//            gotoAnswer(TYPE_CHART_AREA);
//        }
//        else if(text.toString().toLowerCase().contains("circle".toLowerCase()) || text.toString().toLowerCase().contains("Bieu Do".toLowerCase())){
//            gotoAnswer(TYPE_CHART_CIRCLE);
//        }
//        else if(text.toString().toLowerCase().contains("line".toLowerCase()) || text.toString().toLowerCase().contains("Bieu Do".toLowerCase())){
//            gotoAnswer(TYPE_CHART_LINE);
//        }
//        else if(text.toString().toLowerCase().contains("column".toLowerCase()) || text.toString().toLowerCase().contains("Bieu Do".toLowerCase())){
//            gotoAnswer(TYPE_CHART_COLUMN);
//        }
//        else if(text.toString().toLowerCase().contains("stack".toLowerCase()) || text.toString().toLowerCase().contains("Bieu Do".toLowerCase())){
//            gotoAnswer(TYPE_CHART_STACK);
//        }
//        else {
    loadData(Constants.DOMAIN, Constants.TYPE_TEXT, input.getText());
//            loadDataMyViettel(text.toString());
//        }

    return true;
  }

  private void gotoAnswer(String type) {
    if (type.equals(Constants.TYPE_TEXT)) {
      Message message = new Message(MessagesFixtures.getRandomId(), MessagesFixtures.getBotUser(), "Xin chào! Chúc bạn một ngày vui vẻ!");
      messagesAdapter.addToStart(message, true);
    } else if (type.equals(TYPE_IMAGE)) {
      messagesAdapter.addToStart(MessagesFixtures.getImageMessage(), true);
    } else if (type.equals(TYPE_LINK)) {
      messagesAdapter.addToStart(MessagesFixtures.getLinkMessage(), true);
    } else if (type.equals(TYPE_ACTION)) {
      messagesAdapter.addToStart(MessagesFixtures.getActionMessage(), true);
    }
  }

  @Override
  public boolean onVoiceClick() {
    try {
      Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
      intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
          RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
      intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
          "Please speak slowly and enunciate clearly.");
      startActivityForResult(intent, VOICE_RECOGNITION_REQUEST);
    } catch (Exception ex) {
      Logger.log(ex);
      Toast.makeText(this, "Thiết bị không hỗ trợ hoặc đóng disable tính năng này", Toast.LENGTH_LONG).show();
    }
    return true;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == VOICE_RECOGNITION_REQUEST && resultCode == RESULT_OK) {
      ArrayList matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
      final String firstMatch = matches.get(0).toString();
      input.setText(firstMatch);
      new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
          Message message = new Message(MessagesFixtures.getRandomId(), MessagesFixtures.getMyUser(), firstMatch);
          messagesAdapter.addToStart(message, true);
          loadData(Constants.DOMAIN, Constants.TYPE_TEXT, firstMatch);
          input.setText("");
        }
      }, 1000);
    }
  }

  @Override
  public void onAddAttachments() {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        messagesAdapter.addToStart(MessagesFixtures.getImageMessage(), true);
      }
    });
  }

  private void initAdapter() {
//        MessageHolders holdersConfig = new MessageHolders()
//                .setIncomingTextConfig(
//                        CustomIncomingTextMessageViewHolder.class,
//                        R.layout.item_custom_incoming_text_message)
//                .setOutcomingTextConfig(
//                        CustomOutcomingTextMessageViewHolder.class,
//                        R.layout.item_custom_outcoming_text_message)
//                .setIncomingImageConfig(
//                        CustomIncomingImageMessageViewHolder.class,
//                        R.layout.item_custom_incoming_image_message)
//                .setOutcomingImageConfig(
//                        CustomOutcomingImageMessageViewHolder.class,
//                        R.layout.item_custom_outcoming_image_message);

    initNewAdapter();
//        messagesAdapter = new MessagesListAdapter<>(senderId, holdersConfig, imageLoader);
    messagesAdapter.enableSelectionMode(this);
    messagesAdapter.setLoadMoreListener(this);
    messagesAdapter.setOnMessageActionClickListener(this);
    messagesAdapter.setOnMessageButtonClickListener(this);
    messagesAdapter.setOnMessageViewLongClickListener(this);

    messagesAdapter.registerViewClickListener(R.id.messageUserAvatar,
        new MessagesListAdapter.OnMessageViewClickListener<Message>() {
          @Override
          public void onMessageViewClick(View view, Message message) {
//                        AppUtils.showToast(ChatBot.this,
//                                message.getUser().getName() + " avatar click",
//                                false);
          }
        });
    this.messagesList.setAdapter(messagesAdapter);
  }

  public void initNewAdapter() {
    messagesAdapter = new MessagesListAdapter<>(getBaseContext(), senderId, imageLoader);
  }

  @Override
  protected void onStart() {
    super.onStart();
    //Lay du lieu cache
//        messagesAdapter.addToStart(MessagesFixtures.getTextMessage(), true);
  }

  @Override
  public void onBackPressed() {
    if (selectionCount == 0) {
      super.onBackPressed();
    } else {
      messagesAdapter.unselectAllItems();
    }
  }

  @Override
  public void onLoadMore(int page, int totalItemsCount) {
    if (totalItemsCount < TOTAL_MESSAGES_COUNT) {
//            loadMessages();
    }
  }

  protected void loadMessages() {
    new Handler().postDelayed(new Runnable() { //imitation of internet connection
      @Override
      public void run() {
        ArrayList<Message> messages = MessagesFixtures.getMessages(lastLoadedDate);
        lastLoadedDate = messages.get(messages.size() - 1).getCreatedAt();
        messagesAdapter.addToEnd(messages, false);
      }
    }, 1000);
  }

  @Override
  protected void onResume() {
    super.onResume();
    active = true;
  }

  @Override
  protected void onPause() {
    super.onPause();
    active = false;
  }

  @Override
  protected void onDestroy() {
    active = false;
    if (appRtcClient != null)
      appRtcClient.disconnect();
    appRtcClient = null;
    super.onDestroy();
  }

  public boolean isActive() {
    return active;
  }

  private void hideKeBoard() {
    // Check if no view has focus:
    if (ChatBot.this == null)
      return;
    if (ChatBot.this instanceof Activity) {
      View view = ((Activity) ChatBot.this).getCurrentFocus();
      if (view != null) {
        InputMethodManager imm = (InputMethodManager) ChatBot.this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
      }
    }
  }

  @Override
  public void onSelectionChanged(int count) {
//        this.selectionCount = count;
//        menu.findItem(R.id.action_delete).setVisible(count > 0);
//        menu.findItem(R.id.action_copy).setVisible(count > 0);
    this.selectionCount = count;
    if (count > 0)
      btnCopy.setVisibility(View.VISIBLE);
    else
      btnCopy.setVisibility(View.GONE);
  }

  //Xu ly nghiep vu MBI
  public void loadData(String domain, String type, String text) {
    WSRestful restful = new WSRestful(this);
    SenderRequest sender = new SenderRequest(domain, null);
    MessageRequest message = new MessageRequest(text, type, "", null);
    restful.chatbotMessage(headers, new ChatBotRequest(sender, message, null), new Callback<ChatBotResponse>() {
      @Override
      public void onResponse(Call<ChatBotResponse> call, final Response<ChatBotResponse> response) {

        if (response.isSuccessful()) {
          if (response != null && response.body() != null && response.body().getResponse() != null) {
            if (response.body().getType().equals(Constants.TYPE_METADATA)) {
              loadDataMBI(response.body().getResponse().getParameters());
            } else if (response.body().getType().equals(Constants.TYPE_ACTION)) {
              runOnUiThread(new Runnable() {
                @Override
                public void run() {
                  final Message message1 = new Message(MessagesFixtures.getRandomId(), getBotUser(), response.body().getResponse().getSpeech());
                  messagesAdapter.addToStart(message1, true);
                }
              });


              final Message message = new Message(MessagesFixtures.getRandomId(), getBotUser(), null);
              message.setText(response.body().getResponse().getSpeech());
              Message.QuickReply quickReplies = response.body().getResponse().getMessageAction();
              message.setMessageAction(quickReplies);
              runOnUiThread(new Runnable() {
                @Override
                public void run() {
                  messagesAdapter.addToStart(message, true);
                }
              });
            } else if (response.body().getType().equals(Constants.TYPE_TEXT)) {
              String type = response.body().getType();
              if (type.equals(Constants.TYPE_TEXT)) {
                Message message = new Message(MessagesFixtures.getRandomId(), getBotUser(), response.body().getResponse().getSpeech());
                messagesAdapter.addToStart(message, true);
              }
            }
          }
        } else {
          executeErrorResponse(response);
        }
        if (response.body() != null && response.body().getResponse() != null
            && !response.body().getType().equals(Constants.TYPE_METADATA)) {
          messagesAdapter.removeBotLoading();
        }
      }

      @Override
      public void onFailure(Call<ChatBotResponse> call, Throwable t) {
        messagesAdapter.removeBotLoading();
      }
    });
  }

  public void executeErrorResponse(Response<ChatBotResponse> response) {
    new WSRestful(this).resetServiceAll();
  }

  public void loadDataMBI(final ChatBotResponse.Parameters request) {
    WSRestful restful = new WSRestful(this);
    restful.mBIMessage(headers, request, new Callback<ChatBotResponse>() {
      @Override
      public void onResponse(Call<ChatBotResponse> call, Response<ChatBotResponse> response) {
        if (response != null && response.body() != null) {
          String type = response.body().getType();
          if (type.equals(Constants.TYPE_TEXT)) {
            Message message = new Message(MessagesFixtures.getRandomId(), getBotUser(), response.body().getResponse().getSpeech());
            messagesAdapter.addToStart(message, true);
          }
        }
      }

      @Override
      public void onFailure(Call<ChatBotResponse> call, Throwable t) {

      }
    });
  }

  @Override
  public void onMessageQuickReplyClick(QuickReply message, int pos) {
    //Quick reply
    final Message messageRequest = new Message(MessagesFixtures.getRandomId(), MessagesFixtures.getMyUser()
        , message.getTitle());
//        messagesAdapter.addToStart(messageRequest, true);
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        messagesAdapter.addToStart(messageRequest, true);
      }
    });

    loadData(Constants.DOMAIN, Constants.TYPE_TEXT, message.getPayload().toString());
  }

  @Override
  public void onMessageClick(Button message) {
    final Message messageRequest = new Message(MessagesFixtures.getRandomId(), MessagesFixtures.getMyUser(), message.getTitle());
//        messagesAdapter.addToStart(messageRequest, true);
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        messagesAdapter.addToStart(messageRequest, true);
      }
    });
    if (appRtcClient != null)
      appRtcClient.sendEContact(message.getTitle(), message.getListAction().get(0).getPayload().toString(), "mid.1457764197618:41d102a3e1ae206a38");
  }

  private MessagesListAdapter.Formatter<Message> getMessageStringFormatter() {
    return new MessagesListAdapter.Formatter<Message>() {
      @Override
      public String format(Message message) {
//                String createdAt = new SimpleDateFormat("MMM d, EEE 'at' h:mm a", Locale.getDefault())
//                        .format(message.getCreatedAt());
//
//                String text = message.getText();
//                if (text == null) text = "[attachment]";
//
//                return String.format(Locale.getDefault(), "%s: %s (%s)",
//                        message.getUser().getName(), text, createdAt);

        return message.getText();
      }
    };
  }

  @Override
  public void onMessageViewLongClick(View view, Message message) {

  }
}