package com.vtt.chatlib.chatdetail.holders.messages;

import android.view.View;

import com.vtt.chatlib.chatdetail.data.model.Message;
import com.vtt.chatlib.chatkit.messages.MessageHolders;

public class CustomOutcomingTextMessageViewHolder
    extends MessageHolders.OutcomingTextMessageViewHolder<Message> {

  public CustomOutcomingTextMessageViewHolder(View itemView) {
    super(itemView);
  }

  @Override
  public void onBind(Message message) {
    super.onBind(message);

    time.setText(message.getStatus() + " " + time.getText());
  }
}
