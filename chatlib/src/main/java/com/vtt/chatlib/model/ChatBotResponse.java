package com.vtt.chatlib.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vtt.chatlib.chatdetail.data.model.AvailableFeature;
import com.vtt.chatlib.chatdetail.data.model.ConclusionMessage;
import com.vtt.chatlib.chatdetail.data.model.Message;
import com.vtt.chatlib.mbi.dto.BaseItemService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HaiKE on 10/18/17.
 */

public class ChatBotResponse implements Serializable {

  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("response")
  @Expose
  private Response response;
  @SerializedName("timestamp")
  @Expose
  private long timestamp;
  @SerializedName("status")
  @Expose
  private Status status;

  private final static long serialVersionUID = -1374503503550789302L;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Response getResponse() {
    return response;
  }

  public void setResponse(Response response) {
    this.response = response;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "ChatBotResponse{" +
        "type='" + type + '\'' +
        ", response=" + response +
        ", timestamp=" + timestamp +
        ", status=" + status +
        '}';
  }

  public class Parameters implements Serializable {

    @SerializedName("serviceIds")
    @Expose
    private String serviceIds;
    @SerializedName("serviceType")
    @Expose
    private String serviceType;
    @SerializedName("time")
    @Expose
    private Time time;
    @SerializedName("businessUnit")
    @Expose
    private BusinessUnit businessUnit;
    @SerializedName("timeEquivalent")
    @Expose
    private Object timeEquivalent;
    @SerializedName("channelId")
    @Expose
    private String channelId;
    private final static long serialVersionUID = -4043229898252559758L;

    public String getChannelId() {
      return channelId;
    }

    public void setChannelId(String channelId) {
      this.channelId = channelId;
    }

    public String getServiceIds() {
      return serviceIds;
    }

    public void setServiceIds(String serviceIds) {
      this.serviceIds = serviceIds;
    }

    public String getServiceType() {
      return serviceType;
    }

    public void setServiceType(String serviceType) {
      this.serviceType = serviceType;
    }

    public Time getTime() {
      return time;
    }

    public void setTime(Time time) {
      this.time = time;
    }

    public BusinessUnit getBusinessUnit() {
      return businessUnit;
    }

    public void setBusinessUnit(BusinessUnit businessUnit) {
      this.businessUnit = businessUnit;
    }

    public Object getTimeEquivalent() {
      return timeEquivalent;
    }

    public void setTimeEquivalent(Object timeEquivalent) {
      this.timeEquivalent = timeEquivalent;
    }

    @Override
    public String toString() {
      return "Parameters{" +
          "serviceIds='" + serviceIds + '\'' +
          ", serviceType='" + serviceType + '\'' +
          ", time=" + time +
          ", businessUnit=" + businessUnit +
          ", timeEquivalent=" + timeEquivalent +
          ", channelId='" + channelId + '\'' +
          '}';
    }
  }

  public class Response implements Serializable {

    @SerializedName("resolvedQuery")
    private String resolvedQuery;
    @SerializedName("tag")
    private String tag;
    @SerializedName("parameters")
    private Parameters parameters;

    @SerializedName("messageAction")
    private Message.QuickReply messageAction;

    @SerializedName("chart")
    private BaseItemService chart;
    @SerializedName("speech")
    private String speech;
    @SerializedName("context")
    private Object context;

    @SerializedName("cardActions")
    private ArrayList<AvailableFeature> availableFeatures;

    @SerializedName("redirectAction")
    private ConclusionMessage conclusionMessage;
    @SerializedName("payload")
    private Object payload;

    public BaseItemService getChart() {
      return chart;
    }

    public void setChart(BaseItemService chart) {
      this.chart = chart;
    }

    private final static long serialVersionUID = -5205787623332599104L;

    public String getResolvedQuery() {
      return resolvedQuery;
    }

    public void setResolvedQuery(String resolvedQuery) {
      this.resolvedQuery = resolvedQuery;
    }

    public String getTag() {
      return tag;
    }

    public void setTag(String tag) {
      this.tag = tag;
    }

    public Parameters getParameters() {
      return parameters;
    }

    public void setParameters(Parameters parameters) {
      this.parameters = parameters;
    }

    public String getSpeech() {
      return speech;
    }

    public void setSpeech(String speech) {
      this.speech = speech;
    }

    public Message.QuickReply getMessageAction() {
      return messageAction;
    }

    public void setMessageAction(Message.QuickReply messageAction) {
      this.messageAction = messageAction;
    }

    public ArrayList<AvailableFeature> getAvailableFeatures() {
      return availableFeatures;
    }

    public void setAvailableFeatures(ArrayList<AvailableFeature> availableFeatures) {
      this.availableFeatures = availableFeatures;
    }

    public ConclusionMessage getConclusionMessage() {
      return conclusionMessage;
    }

    public void setConclusionMessage(ConclusionMessage conclusionMessage) {
      this.conclusionMessage = conclusionMessage;
    }

    public Object getContext() {
      return context;
    }

    public void setContext(Object context) {
      this.context = context;
    }

    public Object getPayload() {
      return payload;
    }

    public void setPayload(Object payload) {
      this.payload = payload;
    }

    @Override
    public String toString() {
      return "Response{" +
          "resolvedQuery='" + resolvedQuery + '\'' +
          ", message=" + speech +
          ", tag='" + tag + '\'' +
          ", parameters=" + parameters +
          '}';
    }
  }

  public class Status implements Serializable {

    @SerializedName("code")
    @Expose
    private long code;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("description")
    @Expose
    private Object description;
    private final static long serialVersionUID = 7404740662749069384L;

    public long getCode() {
      return code;
    }

    public void setCode(long code) {
      this.code = code;
    }

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public Object getDescription() {
      return description;
    }

    public void setDescription(Object description) {
      this.description = description;
    }

    @Override
    public String toString() {
      return "Status{" +
          "code=" + code +
          ", type='" + type + '\'' +
          ", description=" + description +
          '}';
    }
  }

  public class Time implements Serializable {

    @SerializedName("cycle")
    @Expose
    private String cycle;
    @SerializedName("start")
    @Expose
    private Object start;
    @SerializedName("end")
    @Expose
    private String end;
    @SerializedName("selected")
    @Expose
    private Object selected;
    @SerializedName("absolute")
    @Expose
    private Boolean absolute;
    @SerializedName("code")
    @Expose
    private Object code;
    @SerializedName("title")
    @Expose
    private Object title;
    @SerializedName("text")
    @Expose
    private String text;
    private final static long serialVersionUID = 4531903216738796874L;

    public String getCycle() {
      return cycle;
    }

    public void setCycle(String cycle) {
      this.cycle = cycle;
    }

    public Object getStart() {
      return start;
    }

    public void setStart(Object start) {
      this.start = start;
    }

    public String getEnd() {
      return end;
    }

    public void setEnd(String end) {
      this.end = end;
    }

    public Object getSelected() {
      return selected;
    }

    public void setSelected(Object selected) {
      this.selected = selected;
    }

    public Boolean getAbsolute() {
      return absolute;
    }

    public void setAbsolute(Boolean absolute) {
      this.absolute = absolute;
    }

    public Object getCode() {
      return code;
    }

    public void setCode(Object code) {
      this.code = code;
    }

    public Object getTitle() {
      return title;
    }

    public void setTitle(Object title) {
      this.title = title;
    }

    public String getText() {
      return text;
    }

    public void setText(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return "Time{" +
          "cycle='" + cycle + '\'' +
          ", start=" + start +
          ", end='" + end + '\'' +
          ", selected=" + selected +
          ", absolute=" + absolute +
          ", code=" + code +
          ", title=" + title +
          ", text='" + text + '\'' +
          '}';
    }
  }

  public class BusinessUnit implements Serializable {

    @SerializedName("codes")
    @Expose
    private String codes;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("listUnitIds")
    @Expose
    private List<String> listUnitIds = null;
    private final static long serialVersionUID = -3955249932899715616L;

    public String getCodes() {
      return codes;
    }

    public void setCodes(String codes) {
      this.codes = codes;
    }

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public List<String> getListUnitIds() {
      return listUnitIds;
    }

    public void setListUnitIds(List<String> listUnitIds) {
      this.listUnitIds = listUnitIds;
    }

    @Override
    public String toString() {
      return "BusinessUnit{" +
          "codes='" + codes + '\'' +
          ", type='" + type + '\'' +
          ", listUnitIds=" + listUnitIds +
          '}';
    }
  }
}
