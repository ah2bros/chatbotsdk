package com.vtt.chatlib.network.socket.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by HaiKE on 10/23/17.
 */

public class EContactResponse {
  @SerializedName("dialog-id")
  @Expose
  private String dialogId;
  @SerializedName("account")
  @Expose
  private String account;
  @SerializedName("mid")
  @Expose
  private String mid;
  @SerializedName("time-stamp")
  @Expose
  private String timeStamp;
  @SerializedName("app_name")
  @Expose
  private String appName;
  @SerializedName("app_dialog_id")
  @Expose
  private String appDialogId;
  @SerializedName("action")
  @Expose
  private String action;
  @SerializedName("answers")
  @Expose
  private List<Answer> answers = null;
  private final static long serialVersionUID = -4803498548275367796L;

  public String getDialogId() {
    return dialogId;
  }

  public void setDialogId(String dialogId) {
    this.dialogId = dialogId;
  }

  public String getAccount() {
    return account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getMid() {
    return mid;
  }

  public void setMid(String mid) {
    this.mid = mid;
  }

  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public String getAppDialogId() {
    return appDialogId;
  }

  public void setAppDialogId(String appDialogId) {
    this.appDialogId = appDialogId;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public List<Answer> getAnswers() {
    return answers;
  }

  public void setAnswers(List<Answer> answers) {
    this.answers = answers;
  }

  @Override
  public String toString() {
    return "EContactResponse{" +
        "dialogId='" + dialogId + '\'' +
        ", account='" + account + '\'' +
        ", mid='" + mid + '\'' +
        ", timeStamp='" + timeStamp + '\'' +
        ", appName='" + appName + '\'' +
        ", appDialogId='" + appDialogId + '\'' +
        ", action='" + action + '\'' +
        ", answers=" + answers +
        '}';
  }

  public class Answer implements Serializable {

    @SerializedName("answer-id")
    @Expose
    private String answerId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("quick_replies")
    @Expose
    private List<QuickReply> quickReplies = null;
    @SerializedName("attachments")
    @Expose
    private List<Attachment> attachments = null;
    private final static long serialVersionUID = -1163129522728404642L;

    public String getAnswerId() {
      return answerId;
    }

    public void setAnswerId(String answerId) {
      this.answerId = answerId;
    }

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public String getText() {
      return text;
    }

    public void setText(String text) {
      this.text = text;
    }

    public List<QuickReply> getQuickReplies() {
      return quickReplies;
    }

    public void setQuickReplies(List<QuickReply> quickReplies) {
      this.quickReplies = quickReplies;
    }

    public List<Attachment> getAttachments() {
      return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
      this.attachments = attachments;
    }

    @Override
    public String toString() {
      return "Answer{" +
          "answerId='" + answerId + '\'' +
          ", type='" + type + '\'' +
          ", text='" + text + '\'' +
          ", quickReplies=" + quickReplies +
          ", attachments=" + attachments +
          '}';
    }
  }

  public class Attachment implements Serializable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("subtitle")
    @Expose
    private String subtitle;
    @SerializedName("buttons")
    @Expose
    private List<Button> buttons = null;
    private final static long serialVersionUID = -8370204072477801615L;

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public String getSubtitle() {
      return subtitle;
    }

    public void setSubtitle(String subtitle) {
      this.subtitle = subtitle;
    }

    public List<Button> getButtons() {
      return buttons;
    }

    public void setButtons(List<Button> buttons) {
      this.buttons = buttons;
    }

    @Override
    public String toString() {
      return "Attachment{" +
          "title='" + title + '\'' +
          ", subtitle='" + subtitle + '\'' +
          ", buttons=" + buttons +
          '}';
    }
  }

  public class Button implements Serializable {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("payload")
    @Expose
    private String payload;
    private final static long serialVersionUID = 4780301764077233673L;

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public String getPayload() {
      return payload;
    }

    public void setPayload(String payload) {
      this.payload = payload;
    }

    @Override
    public String toString() {
      return "ActionList{" +
          "type='" + type + '\'' +
          ", title='" + title + '\'' +
          ", payload='" + payload + '\'' +
          '}';
    }
  }

  public class QuickReply implements Serializable {

    @SerializedName("content_type")
    @Expose
    private String contentType;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("payload")
    @Expose
    private String payload;
    private final static long serialVersionUID = -4864635135379001951L;

    public String getContentType() {
      return contentType;
    }

    public void setContentType(String contentType) {
      this.contentType = contentType;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public String getPayload() {
      return payload;
    }

    public void setPayload(String payload) {
      this.payload = payload;
    }

    @Override
    public String toString() {
      return "QuickReply{" +
          "contentType='" + contentType + '\'' +
          ", title='" + title + '\'' +
          ", payload='" + payload + '\'' +
          '}';
    }
  }
}
