package com.vtt.chatlib.network.socket.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by HaiKE on 10/18/17.
 */


public class SenderRequest implements Serializable {

  @SerializedName("app_name")
  @Expose
  private String appName;
  @SerializedName("account")
  @Expose
  private String account;
  @SerializedName("access_token")
  @Expose
  private String accessToken;
  @SerializedName("msisdn")
  @Expose
  private String msisdn;
  private final static long serialVersionUID = -5888657946754484507L;

  public SenderRequest(String appName, String account, String accessToken, String msisdn) {
    this.appName = appName;
    this.account = account;
    this.accessToken = accessToken;
    this.msisdn = msisdn;
  }

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public String getAccount() {
    return account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getMsisdn() {
    return msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  @Override
  public String toString() {
    return "Sender{" +
        "appName='" + appName + '\'' +
        ", account='" + account + '\'' +
        ", accessToken='" + accessToken + '\'' +
        ", msisdn='" + msisdn + '\'' +
        '}';
  }
}