package com.vtt.chatlib.network;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vtt.chatlib.ChatBotUtils;
import com.vtt.chatlib.chatdetail.ChatBot;
import com.vtt.chatlib.mbi.deserializer.ItemServiceDeserializer;
import com.vtt.chatlib.mbi.dto.BaseItemService;
import com.vtt.chatlib.model.ChatBotRequest;
import com.vtt.chatlib.model.ChatBotResponse;
import com.vtt.chatlib.utils.Logger;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by HaiKE on 8/22/16.
 */
public class WSRestful {
  public static final String DOMAIN_MY_VIETTEL = "myviettel";

  private static final String TAG = WSRestful.class.getSimpleName();
  private static final String CACHE_CONTROL = "Cache-Control";
  private APIClient service;
  private APIClient serviceMBI;
  protected Context context;

  public WSRestful(Context context) {
    this.context = context;
  }

  public APIClient getIstance(Context context, HashMap<String, String> headers) {
    if (service == null) {
//            OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
//            builder.readTimeout(15, TimeUnit.SECONDS);
//            builder.connectTimeout(10, TimeUnit.SECONDS);
//            builder.writeTimeout(10, TimeUnit.SECONDS);
//            if (BuildConfig.DEBUG) {
//                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//                builder.addInterceptor(interceptor);
//            }
//            int cacheSize = 50 * 1024 * 1024; // 50 MiB
//            Cache cache = new Cache(context.getCacheDir(), cacheSize);
//            builder.cache(cache);

      Gson gson = new GsonBuilder()
          .registerTypeAdapter(BaseItemService.class, new ItemServiceDeserializer())
          .create();

      Retrofit retrofit = new Retrofit.Builder()
          .client(provideOkHttpClient(headers))
          .addConverterFactory(GsonConverterFactory.create(gson))
          .baseUrl(ChatBotUtils.ENDPOINT_CHATBOT)
          .build();

      service = retrofit.create(APIClient.class);
      return service;
    } else {
      return service;
    }
  }

  public APIClient getIstanceMBI(Context context, HashMap<String, String> headers) {
    if (serviceMBI == null) {
//            OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
//            builder.readTimeout(15, TimeUnit.SECONDS);
//            builder.connectTimeout(10, TimeUnit.SECONDS);
//            builder.writeTimeout(10, TimeUnit.SECONDS);
//            if (BuildConfig.DEBUG) {
//                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//                builder.addInterceptor(interceptor);
//            }
//            int cacheSize = 50 * 1024 * 1024; // 50 MiB
//            Cache cache = new Cache(context.getCacheDir(), cacheSize);
//            builder.cache(cache);

      Retrofit retrofit = new Retrofit.Builder()
          .client(provideOkHttpClient(headers))
          .addConverterFactory(GsonConverterFactory.create())
          .baseUrl(ChatBotUtils.ENDPOINT_MBI)
          .build();

      serviceMBI = retrofit.create(APIClient.class);
      return serviceMBI;
    } else {
      return serviceMBI;
    }
  }

  private OkHttpClient provideOkHttpClient(final HashMap<String, String> headers) {
    return new OkHttpClient.Builder()
        .addInterceptor(provideHttpLoggingInterceptor())
        .addInterceptor(new Interceptor() {
          @Override
          public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request.Builder builder = original.newBuilder();
            if (headers != null)
              for (Map.Entry<String, String> header : headers.entrySet()) {
                builder.header(header.getKey(), header.getValue());
              }

            Request request = builder
                .method(original.method(), original.body())
                .build();

            return chain.proceed(request);
          }
        })
        .cache(provideCache())
        .build();
  }

  private OkHttpClient provideOkHttpClient() {
    return new OkHttpClient.Builder()
        .addInterceptor(provideHttpLoggingInterceptor())
//                .addInterceptor(provideOfflineCacheInterceptor())
//                .addNetworkInterceptor(provideCacheInterceptor())
        .cache(provideCache())
        .build();
  }

  private Cache provideCache() {
    Cache cache = null;
    try {
      cache = new Cache(new File(context.getCacheDir(), "http-cache"), 10 * 1024 * 1024); // 10 MB
    } catch (Exception e) {
      Logger.log(e);
      Log.e(TAG, "Could not create Cache!");
    }
    return cache;
  }

  private HttpLoggingInterceptor provideHttpLoggingInterceptor() {
//        HttpLoggingInterceptor httpLoggingInterceptor =
//                new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
//                    @Override
//                    public void log(String message) {
//                        Log.d(TAG, message);
//                    }
//                });
    HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
    httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        httpLoggingInterceptor.setLevel(BuildConfig.DEBUG ? HEADERS : NONE);
    return httpLoggingInterceptor;
  }

//    public Interceptor provideCacheInterceptor() {
//        return new Interceptor() {
//            @Override
//            public Response intercept(Chain chain) throws IOException {
//                Response response = chain.proceed(chain.request());
//
//                // re-write response header to force use of cache
//                CacheControl cacheControl = new CacheControl.Builder()
//                        .maxAge(2, TimeUnit.MINUTES)
//                        .build();
//
//                return response.newBuilder()
//                        .header(CACHE_CONTROL, cacheControl.toString())
//                        .build();
//            }
//        };
//    }
//
//    public Interceptor provideOfflineCacheInterceptor() {
//        return new Interceptor() {
//            @Override
//            public Response intercept(Chain chain) throws IOException {
//                Request request = chain.request();
//
//                if (!ConnectivityUtil.isConnected(context)) {
//                    CacheControl cacheControl = new CacheControl.Builder()
//                            .maxStale(7, TimeUnit.DAYS)
//                            .build();
//
//                    request = request.newBuilder()
//                            .cacheControl(cacheControl)
//                            .build();
//                }
//
//                return chain.proceed(request);
//            }
//        };
//    }

  public void resetServiceMBI() {
    serviceMBI = null;
  }

  public void resetServiceAll() {
    service = null;
    serviceMBI = null;
  }

  public void chatbotMessage(HashMap<String, String> headers, ChatBotRequest request, Callback<ChatBotResponse> callback) {
    APIClient apiClient = getIstance(context, headers);
    apiClient.chatbotMessage(request).enqueue(callback);
  }

  public void mBIMessage(HashMap<String, String> headers, ChatBotResponse.Parameters request, Callback<ChatBotResponse> callback) {
    APIClient apiClient = getIstanceMBI(context, headers);
    apiClient.mBIMessage(request).enqueue(callback);
  }
}
