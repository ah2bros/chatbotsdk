package com.vtt.chatlib.network.socket.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HaiKE on 10/30/17.
 */

public class EContactConversation {
  @SerializedName("message")
  @Expose
  private String message;

  @SerializedName("res")
  @Expose
  private String code;

  @SerializedName("userName")
  @Expose
  private String userName;

  @SerializedName("conversationId")
  @Expose
  private int conversationId;

  @SerializedName("isFromChatbot")
  @Expose
  private int isNeedShow;

  @SerializedName("isTyping")
  @Expose
  private boolean isTyping;

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public boolean isTyping() {
    return isTyping;
  }

  public void setTyping(boolean typing) {
    isTyping = typing;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public int getConversationId() {
    return conversationId;
  }

  public void setConversationId(int conversationId) {
    this.conversationId = conversationId;
  }

  public int getIsNeedShow() {
    return isNeedShow;
  }

  public void setIsNeedShow(int isNeedShow) {
    this.isNeedShow = isNeedShow;
  }
}
