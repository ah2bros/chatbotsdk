package com.vtt.chatlib.network.socket;/*
 *  Copyright 2014 The WebRTC Project Authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

import android.util.Log;

import com.vtt.chatlib.network.socket.model.EContactConversation;
import com.vtt.chatlib.network.socket.model.EContactCustomer;
import com.vtt.chatlib.network.socket.model.EContactMessage;
import com.vtt.chatlib.network.socket.model.EContactResponse;
import com.vtt.chatlib.network.socket.model.MessageData;
import com.vtt.chatlib.utils.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * WebSocket client implementation.
 * <p>
 * <p>All public methods should be called from a looper executor thread
 * passed in a constructor, otherwise exception will be thrown.
 * All events are dispatched on the same thread.
 */

public class WebSocketRTCClient implements AppRTCClient, WebSocketChannelClient.WebSocketChannelEvents {

  private static final String TAG = "WebSocketRTCClient";

  private enum ConnectionState {
    NEW, CONNECTED, CLOSED, ERROR
  }

  ;

//  private enum MessageType {
//    MESSAGE, LEAVE, PING
//  }
//
//  ;

  private final LooperExecutor executor;
  private boolean initiator;

  private WebSocketChannelClient wsClient;
  private WebSocketChannelClient.WebSocketConnectionState socketState;
  private RoomConnectionParameters connectionParameters;

  private SignalingEvents signalingEvents;
  private String domain;
  private String serviceId;
  String appName;
  String account;
  String accessToken;
  String msisdn;
  String token;
  String mid;

  public WebSocketRTCClient(SignalingEvents events, LooperExecutor executor) {
    this.executor = executor;
    this.socketState = WebSocketChannelClient.WebSocketConnectionState.NEW;
    this.signalingEvents = events;

    executor.requestStart();
  }

  // --------------------------------------------------------------------
  // WebSocketChannelEvents interface implementation.
  // All events are called by WebSocketChannelClient on a local looper thread
  // (passed to WebSocket client constructor).
  @Override
  public void onWebSocketMessage(final String msg) {
    try {

      JSONObject json = new JSONObject(msg);

      String serviceType = json.getString("AppServiceType");
      if ("CUSTOMER_LOGIN".equals(serviceType)) {
        signalingEvents.loginSuccess();
      } else if ("MESSAGE".equals(serviceType)) {
        if (json.has("type")) {
          int type = json.getInt("type");
          switch (type) {
            case 5:
              JSONObject data = json.getJSONObject("data");
              EContactMessage response = GsonWrapper.getGson().fromJson(data.toString(), EContactMessage.class);
              if (response.isNeedShow())
                signalingEvents.updateChatBot(response);
              break;
          }
        }
      } else if ("SEND_IS_CHAT_TYPING".equals(serviceType)) {
        if (json.has("type")) {
          int type = json.getInt("type");
          switch (type) {
            case 106:
              JSONObject data = json.getJSONObject("data");
              EContactConversation response = GsonWrapper.getGson().fromJson(data.toString(), EContactConversation.class);
              if (response.isTyping())
                signalingEvents.updateTyping();
              break;
          }
        }
      } else if ("CHATBOT_SEND_SDK".equals(serviceType)) {
        //Da gui

      } else if ("HAVE_MESSAGE".equals(serviceType)) {
        if (json.has("type")) {
          int type = json.getInt("type");
          switch (type) {
            case 4:
              JSONObject data = json.getJSONObject("data");
              EContactCustomer response = GsonWrapper.getGson().fromJson(data.toString(), EContactCustomer.class);
              signalingEvents.updateChatBot(response);
              break;
          }
        }
      } else if ("CHATBOT_RESPONSE_SDK".equals(serviceType)) {
        if (json.has("type")) {
          int type = json.getInt("type");
          switch (type) {
            case 134:
              JSONObject data = json.getJSONObject("data");
              String message = data.getString("message");
              EContactResponse response = GsonWrapper.getGson().fromJson(message, EContactResponse.class);
              signalingEvents.updateChatBot(response);
              break;
          }
        }
      } else if ("WARNING_CUSTOMER_TIMEOUT".equals(serviceType)) {
        if (json.has("type")) {
          int type = json.getInt("type");
          switch (type) {
            case 107:
              JSONObject data = json.getJSONObject("data");
              EContactConversation response = GsonWrapper.getGson().fromJson(data.toString(), EContactConversation.class);
              if (response.getIsNeedShow() == 1)
                signalingEvents.updateChatBot(response);
              break;
          }
        }
      } else if ("LEAVE_CONVERSATION".equals(serviceType)) {
        if (json.has("type")) {
          int type = json.getInt("type");
          switch (type) {
            case 9:
              JSONObject data = json.getJSONObject("data");
              EContactConversation response = GsonWrapper.getGson().fromJson(data.toString(), EContactConversation.class);
              if (response.getIsNeedShow() == 1)
                signalingEvents.updateChatBot(response);
              break;
          }
        }
      }


    } catch (Exception e) {
      Logger.log(e);
      reportError("WebSocket message JSON parsing error: " + e.toString());
    }
  }

  // --------------------------------------------------------------------
  // AppRTCClient interface implementation.
  // Asynchronously connect to an AppRTC room URL using supplied connection
  // parameters, retrieves room parameters and connect to WebSocket server.
  @Override
  public void connectToWebsocket(RoomConnectionParameters connectionParameters) {
    this.connectionParameters = connectionParameters;
    executor.execute(new Runnable() {
      @Override
      public void run() {
        try {
          connectToWebsocketInternal();
        } catch (Exception e) {
          Logger.log(e);
          reportError("WebSocketerror: " + e.toString());
        }
      }
    });
  }

  public void sendStopToPeer() {
    executor.execute(new Runnable() {
      @Override
      public void run() {
        try {
          JSONObject jsonMessage = new JSONObject();
          jsonPut(jsonMessage, "id", "stop");

          wsClient.send(jsonMessage.toString());
        } catch (Exception e) {
          Logger.log(e);
          reportError("WebSocketerror: " + e.toString());
        }
      }
    });
  }

  @Override
  public void sendDisconnectToPeer() {
    executor.execute(new Runnable() {
      @Override
      public void run() {

      }
    });
  }

  @Override
  public void reconnect() {
    executor.execute(new Runnable() {
      @Override
      public void run() {

        try {
          connectToWebsocketInternal();
        } catch (Exception e) {
          Logger.log(e);
          reportError("WebSocketerror: " + e.toString());
        }
      }
    });
  }

  @Override
  public void initUser() {
    //get info
    executor.execute(new Runnable() {
      @Override
      public void run() {
        JSONObject data = new JSONObject();
        WebSocketRTCClient.jsonPut(data, "visitorName", "");
//                WebSocketRTCClient.jsonPut(data, "ip_address", "192.168.0.132:8939");
        WebSocketRTCClient.jsonPut(data, "host", "192.168.0.132:8939");
        WebSocketRTCClient.jsonPut(data, "os", "Android 4.4");
        WebSocketRTCClient.jsonPut(data, "browser", "");
        WebSocketRTCClient.jsonPut(data, "device_type", "");
        WebSocketRTCClient.jsonPut(data, "domain", "MYVIETTEL");
        WebSocketRTCClient.jsonPut(data, "country_name", "VN");
        JSONObject json = new JSONObject();

        WebSocketRTCClient.jsonPut(json, "service", 37);
        WebSocketRTCClient.jsonPut(json, "data", data);
        jsonPut(json, "token", token);
        wsClient.send(json.toString());
      }
    });

  }

  // Connects to websocket - function runs on a local looper thread.
  private void connectToWebsocketInternal() {

    String connectionUrl = getConnectionUrl(connectionParameters);

    socketState = WebSocketChannelClient.WebSocketConnectionState.NEW;
    wsClient = new WebSocketChannelClient(executor, this);
    wsClient.connect(connectionUrl);
    socketState = WebSocketChannelClient.WebSocketConnectionState.CONNECTED;
    Log.d(TAG, "wsClient connect " + connectionUrl);

  }

  // Helper functions to get connection, sendSocketMessage message and leave message URLs
  private String getConnectionUrl(RoomConnectionParameters connectionParameters) {
    return connectionParameters.roomUrl + "/websocket";//cais anfy sao bi comment? ANh chay lai nhe
  }


  @Override
  public void onWebSocketClose() {
    signalingEvents.onChannelClose();
  }

  @Override
  public void onWebSocketError(String description) {
    reportError("WebSocket error: " + description);
  }

  // --------------------------------------------------------------------
  // Helper functions.
  private void reportError(final String errorMessage) {
    Log.e(TAG, errorMessage);
    executor.execute(new Runnable() {
      @Override
      public void run() {
        if (socketState != WebSocketChannelClient.WebSocketConnectionState.ERROR) {
          socketState = WebSocketChannelClient.WebSocketConnectionState.ERROR;
//                    signalingEvents.onChannelError(errorMessage);
        }
      }
    });
  }

  @Override
  public void onWebSocketConnected() {
    connectToUser(0);
  }

  private void connectToUser(int runTimeMs) {
    initUser();
//    login();
//    String to = "112";
//    roomConnectionParameters.initiator = true;
//    roomConnectionParameters.to = to;
  }

  public WebSocketRTCClient setDomain(String domain) {
    this.domain = domain;
    return this;
  }

  public void setServiceId(String serviceId) {
    this.serviceId = serviceId;
  }

  public void disconnect() {
    if (wsClient != null) {
      wsClient.disconnect();
    }
  }

  public void sendEContact(final String text, final String payload, final String mid) {
    executor.execute(new Runnable() {
      @Override
      public void run() {
//        JSONObject json = new JSONObject();
//        jsonPut(json, "service", "133");
//        jsonPut(json, "app_name", "MYVIETTEL");
//        jsonPut(json, "account", "myviettel");
//        jsonPut(json, "access_token", "myviettel@123");
//        jsonPut(json, "mode", "NORMAL");
//        jsonPut(json, "msisdn", "0969113369");
//        jsonPut(json, "text", text);
//        jsonPut(json, "payload", payload);
//        jsonPut(json, "timestamp", System.currentTimeMillis());
//        jsonPut(json, "mid", "mid.1457764197618:41d102a3e1ae206a38");
//        jsonPut(json, "token", Utils.generateToken());
//        // Call receiver sends ice candidates to websocket server.
//        wsClient.send(json.toString());

        MessageData data = new MessageData();
        data.setAppName(appName);
        data.setAccount(account);
        data.setAccessToken(accessToken);
        data.setMode("NORMAL");
        data.setMsisdn(msisdn);
        data.setText(text);
        data.setPayload(payload);
        data.setTimestamp(System.currentTimeMillis());
        data.setMid(mid);
        data.setToken(token);

        Message message = new Message()
            .setService("133")
            .setData(data)
            .setToken(token);

        // Call receiver sends ice candidates to websocket server.
        wsClient.send(message);

      }
    });
  }

  // Put a |key|->|value| mapping in |json|.
  public static void jsonPut(JSONObject json, String key, Object value) {
    try {
      json.put(key, value);
    } catch (JSONException e) {
      throw new RuntimeException(e);
    }
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public void setMid(String mid) {
    this.mid = mid;
  }
}