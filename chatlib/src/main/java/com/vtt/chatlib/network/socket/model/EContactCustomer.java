package com.vtt.chatlib.network.socket.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by HaiKE on 11/2/17.
 */

public class EContactCustomer implements Serializable {

  @SerializedName("visitorName")
  @Expose
  private String visitorName;
  @SerializedName("picture")
  @Expose
  private String picture;
  @SerializedName("message")
  @Expose
  private String message;
  @SerializedName("r")
  @Expose
  private Integer r;
  @SerializedName("conversationType")
  @Expose
  private String conversationType;
  @SerializedName("conversationId")
  @Expose
  private Integer conversationId;
  @SerializedName("domain")
  @Expose
  private String domain;
  @SerializedName("fileName")
  @Expose
  private String fileName;
  @SerializedName("visitorId")
  @Expose
  private Integer visitorId;
  @SerializedName("fromFullname")
  @Expose
  private String fromFullname;
  @SerializedName("visitorUsername")
  @Expose
  private String visitorUsername;
  @SerializedName("fromUserId")
  @Expose
  private Integer fromUserId;
  @SerializedName("fileUrl")
  @Expose
  private String fileUrl;
  private final static long serialVersionUID = -419982776152842812L;

  public String getVisitorName() {
    return visitorName;
  }

  public void setVisitorName(String visitorName) {
    this.visitorName = visitorName;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Integer getR() {
    return r;
  }

  public void setR(Integer r) {
    this.r = r;
  }

  public String getConversationType() {
    return conversationType;
  }

  public void setConversationType(String conversationType) {
    this.conversationType = conversationType;
  }

  public Integer getConversationId() {
    return conversationId;
  }

  public void setConversationId(Integer conversationId) {
    this.conversationId = conversationId;
  }

  public String getDomain() {
    return domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public Integer getVisitorId() {
    return visitorId;
  }

  public void setVisitorId(Integer visitorId) {
    this.visitorId = visitorId;
  }

  public String getFromFullname() {
    return fromFullname;
  }

  public void setFromFullname(String fromFullname) {
    this.fromFullname = fromFullname;
  }

  public String getVisitorUsername() {
    return visitorUsername;
  }

  public void setVisitorUsername(String visitorUsername) {
    this.visitorUsername = visitorUsername;
  }

  public Integer getFromUserId() {
    return fromUserId;
  }

  public void setFromUserId(Integer fromUserId) {
    this.fromUserId = fromUserId;
  }

  public String getFileUrl() {
    return fileUrl;
  }

  public void setFileUrl(String fileUrl) {
    this.fileUrl = fileUrl;
  }

  @Override
  public String toString() {
    return "EContactCustomer{" +
        "visitorName='" + visitorName + '\'' +
        ", picture='" + picture + '\'' +
        ", message='" + message + '\'' +
        ", r=" + r +
        ", conversationType='" + conversationType + '\'' +
        ", conversationId=" + conversationId +
        ", domain='" + domain + '\'' +
        ", fileName='" + fileName + '\'' +
        ", visitorId=" + visitorId +
        ", fromFullname='" + fromFullname + '\'' +
        ", visitorUsername='" + visitorUsername + '\'' +
        ", fromUserId=" + fromUserId +
        ", fileUrl='" + fileUrl + '\'' +
        '}';
  }
}