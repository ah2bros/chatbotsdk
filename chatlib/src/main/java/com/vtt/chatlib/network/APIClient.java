package com.vtt.chatlib.network;

import com.vtt.chatlib.model.ChatBotRequest;
import com.vtt.chatlib.model.ChatBotResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by HaiKE on 8/22/16.
 */
public interface APIClient {

  @POST("assistant/message")
  Call<ChatBotResponse> chatbotMessage(@Body ChatBotRequest request);

  @POST("vbi/chatbot/data")
  Call<ChatBotResponse> mBIMessage(@Body ChatBotResponse.Parameters request);

}
