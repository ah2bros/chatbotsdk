package com.vtt.chatlib.chatkit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by HaiKE on 10/9/17.
 */

public class QuickReply implements Serializable {
  @SerializedName("type")
  @Expose
  private String contentType;
  @SerializedName("title")
  @Expose
  private String title;
  @SerializedName("payload")
  @Expose
  private Object payload;
  @SerializedName("dest")
  @Expose
  private String dest;

  private final static long serialVersionUID = -2047301391544994810L;

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Object getPayload() {
    return payload;
  }

  public void setPayload(String payload) {
    this.payload = payload;
  }

  public String getDest() {
    return dest;
  }

  public void setDest(String dest) {
    this.dest = dest;
  }

  public QuickReply(String contentType, String title, String payload) {
    this.contentType = contentType;
    this.title = title;
    this.payload = payload;
  }

  @Override
  public String toString() {
    return "QuickReply{" +
        "contentType='" + contentType + '\'' +
        ", title='" + title + '\'' +
        ", payload='" + payload + '\'' +
        '}';
  }
}