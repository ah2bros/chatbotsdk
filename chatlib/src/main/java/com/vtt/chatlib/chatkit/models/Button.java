package com.vtt.chatlib.chatkit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by HaiKE on 10/24/17.
 */

public class Button implements Serializable {
  @SerializedName("title")
  @Expose
  private String title;

  @SerializedName("subtitle")
  @Expose
  private String subtitle;

  private ArrayList<QuickReply> listAction = new ArrayList<>();

  public Button(String title, String subtitle, ArrayList<QuickReply> listAction) {
    this.title = title;
    this.subtitle = subtitle;
    this.listAction = listAction;
  }

  @Override
  public String toString() {
    return "ActionList{" +
        "title='" + title + '\'' +
        ", subtitle='" + subtitle + '\'' +
        ", listAction=" + listAction +
        '}';
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public ArrayList<QuickReply> getListAction() {
    return listAction;
  }

  public void setListAction(ArrayList<QuickReply> listAction) {
    this.listAction = listAction;
  }
}