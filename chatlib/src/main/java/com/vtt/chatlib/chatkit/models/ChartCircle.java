package com.vtt.chatlib.chatkit.models;

/**
 * Created by HaiKE on 10/12/17.
 */

public class ChartCircle {
  String title;
  String color;
  float percent;
  int objectID;
  float value;

  public ChartCircle(String title, String color, float value, float percent, int objectID) {
    this.title = title;
    this.color = color;
    this.percent = percent;
    this.objectID = objectID;
    this.value = value;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public float getPercent() {
    return percent;
  }

  public void setPercent(float percent) {
    this.percent = percent;
  }

  public int getObjectID() {
    return objectID;
  }

  public void setObjectID(int objectID) {
    this.objectID = objectID;
  }
}
