/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.vtt.chatlib.chatkit.messages;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.Space;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vtt.chatlib.ChatBotUtils;
import com.vtt.chatlib.R;
import com.vtt.chatlib.network.WSRestful;
import com.vtt.chatlib.utils.EasyDialog;
import com.vtt.chatlib.utils.Logger;
import com.vtt.chatlib.utils.Utils;

import java.lang.reflect.Field;

/**
 * Component for input outcoming messages
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class MessageInput extends RelativeLayout
    implements View.OnClickListener, TextWatcher {

  protected EditText messageInputEdt;
  protected ImageButton messageSendButton;
  protected ImageButton btnVoice;
  protected ImageButton attachmentButton;
  protected Space sendButtonSpace, attachmentButtonSpace;

  private CharSequence input;
  private InputListener inputListener;
  private AttachmentsListener attachmentsListener;

  public MessageInput(Context context) {
    super(context);
    init(context);
  }

  public MessageInput(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs);
  }

  public MessageInput(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs);
  }

  /**
   * Sets callback for 'submit' button.
   *
   * @param inputListener input callback
   */
  public void setInputListener(InputListener inputListener) {
    this.inputListener = inputListener;
  }

  /**
   * Sets callback for 'add' button.
   *
   * @param attachmentsListener input callback
   */
  public void setAttachmentsListener(AttachmentsListener attachmentsListener) {
    this.attachmentsListener = attachmentsListener;
  }

  /**
   * Returns EditText for messages input
   *
   * @return EditText
   */
  public EditText getInputEditText() {
    return messageInputEdt;
  }

  /**
   * Returns `submit` button
   *
   * @return ImageButton
   */
  public ImageButton getButton() {
    return messageSendButton;
  }

  @Override
  public void onClick(View view) {
    int id = view.getId();
    if (id == R.id.messageSendButton) {
      if (messageInputEdt.getText().length() > 0) {
        submitText();
      } else onVoice();
    } else if (id == R.id.attachmentButton) {
      onAddAttachments();
    } else if (id == R.id.btnVoice) {
      onVoice();
    }
  }

  /**
   * This method is called to notify you that, within s,
   * the count characters beginning at start have just replaced old text that had length before
   */
  @Override
  public void onTextChanged(CharSequence s, int start, int count, int after) {
    input = s;
//    messageSendButton.setEnabled(input.length() > 0);
    if (input.length() > 0) {
      messageSendButton.setImageResource(R.drawable.ic_send_message);
    } else {
      messageSendButton.setImageResource(R.drawable.ic_voice);
    }
    if (ChatBotUtils.DOMAIN.equals(WSRestful.DOMAIN_MY_VIETTEL))
      messageSendButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.incoming_text_message_bg_myvt),
          PorterDuff.Mode.MULTIPLY);
    else
      messageSendButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.incoming_text_message_bg),
          PorterDuff.Mode.MULTIPLY);
  }

  /**
   * This method is called to notify you that, within s,
   * the count characters beginning at start are about to be replaced by new text with length after.
   */
  @Override
  public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    //do nothing
  }

  /**
   * This method is called to notify you that, somewhere within s, the text has been changed.
   */
  @Override
  public void afterTextChanged(Editable editable) {
    //do nothing
  }

  private boolean onSubmit() {
    return inputListener != null && inputListener.onSubmit(input);
  }

  private boolean onVoice() {
    return inputListener != null && inputListener.onVoiceClick();
  }

  private void onAddAttachments() {
    if (attachmentsListener != null) attachmentsListener.onAddAttachments();
  }

  private void init(Context context, AttributeSet attrs) {
    init(context);
    MessageInputStyle style = MessageInputStyle.parse(context, attrs);

    this.messageInputEdt.setMaxLines(style.getInputMaxLines());
    this.messageInputEdt.setHint(style.getInputHint());
    this.messageInputEdt.setText(style.getInputText());
    this.messageInputEdt.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getInputTextSize());
    this.messageInputEdt.setTextColor(style.getInputTextColor());
    this.messageInputEdt.setHintTextColor(style.getInputHintColor());
//        ViewCompat.setBackground(this.messageInputEdt, style.getInputBackground());
    setCursor(style.getInputCursorDrawable());

    this.attachmentButton.setVisibility(style.showAttachmentButton() ? VISIBLE : GONE);
    this.attachmentButton.setImageDrawable(style.getAttachmentButtonIcon());
    this.attachmentButton.getLayoutParams().width = style.getAttachmentButtonWidth();
    this.attachmentButton.getLayoutParams().height = style.getAttachmentButtonHeight();
    ViewCompat.setBackground(this.attachmentButton, style.getAttachmentButtonBackground());

    this.btnVoice.getLayoutParams().width = style.getAttachmentButtonWidth();
    this.btnVoice.getLayoutParams().height = style.getAttachmentButtonHeight();
    ViewCompat.setBackground(btnVoice, style.getAttachmentButtonBackground());

//        this.attachmentButtonSpace.setVisibility(style.showAttachmentButton() ? VISIBLE : GONE);
//    this.attachmentButtonSpace.getLayoutParams().width = style.getAttachmentButtonMargin();

//    this.messageSendButton.setImageDrawable(style.getInputButtonIcon());
    this.messageSendButton.getLayoutParams().width = style.getInputButtonWidth();
    this.messageSendButton.getLayoutParams().height = style.getInputButtonHeight();
//    ViewCompat.setBackground(messageSendButton, style.getInputButtonBackground());

//    this.sendButtonSpace.getLayoutParams().width = style.getInputButtonMargin();
  }

  public void setText(String str) {
    messageInputEdt.setText(str);
  }

  private void init(Context context) {
    inflate(context, R.layout.view_message_input, this);

    messageInputEdt = (EditText) findViewById(R.id.messageInput);
    messageSendButton = (ImageButton) findViewById(R.id.messageSendButton);
    btnVoice = (ImageButton) findViewById(R.id.btnVoice);
    attachmentButton = (ImageButton) findViewById(R.id.attachmentButton);
    sendButtonSpace = (Space) findViewById(R.id.sendButtonSpace);
    attachmentButtonSpace = (Space) findViewById(R.id.attachmentButtonSpace);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      setUpActionPaste();
    }

    messageSendButton.setOnClickListener(this);
    btnVoice.setOnClickListener(this);
    attachmentButton.setOnClickListener(this);
    messageInputEdt.addTextChangedListener(this);
    messageInputEdt.setText("");
    messageInputEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) && event.getAction() == KeyEvent.ACTION_DOWN
            && !messageInputEdt.getText().toString().isEmpty()) {
          submitText();
          return true;
        }
        return false;
      }
    });
  }

  private void submitText() {
    boolean isSubmitted = onSubmit();
    if (isSubmitted) {
      messageInputEdt.setText("");
    }
  }

  public void setUpActionPaste() {
    messageInputEdt.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
      @Override
      public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        return false;
      }

      @Override
      public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
      }

      @Override
      public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        return false;
      }

      @Override
      public void onDestroyActionMode(ActionMode mode) {
      }
    });

    messageInputEdt.setOnLongClickListener(new OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        new Handler().postDelayed(new Runnable() {
          @Override
          public void run() {
            View rateView = LayoutInflater.from(getContext()).inflate(R.layout.view_paste_option, null);
            rateView.findViewById(R.id.txt_paste).setOnClickListener(new OnClickListener() {
              @Override
              public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                if (item.getText() != null && !item.getText().toString().isEmpty()) {
                  String start = messageInputEdt.getText().toString().substring(0, messageInputEdt.getSelectionStart());
                  String end = messageInputEdt.getText().toString().substring(messageInputEdt.getSelectionEnd(), messageInputEdt.getText().toString().length());
                  int endPosition = messageInputEdt.getSelectionEnd();
                  messageInputEdt.setText(start + item.getText() + end);
                  if (endPosition < messageInputEdt.getText().toString().length())
                    endPosition = messageInputEdt.getText().toString().length();
                  messageInputEdt.setSelection(endPosition);
                }

                easyDialog.dismiss();
              }
            });

            int pos = messageInputEdt.getSelectionStart();
            Layout layout = messageInputEdt.getLayout();
            int line = layout.getLineForOffset(pos);
//            int baseline = layout.getLineBaseline(line);
//            int ascent = layout.getLineAscent(line);
            float x = layout.getPrimaryHorizontal(pos);
//            float y = baseline + ascent;
            int[] location = new int[2];
            messageInputEdt.getLocationOnScreen(location);
            location[0] = (int) x + messageInputEdt.getLeft() + ((View) messageInputEdt.getParent()).getLeft();

            showEasyDialogNoTriagle(getContext(), rateView, location);
          }
        }, 100);
        return false;
      }
    });
  }

  static EasyDialog easyDialog;

  public static synchronized void showEasyDialogNoTriagle(final Context context, View layout, View anchorView) {
    easyDialog = new EasyDialog(context).setLayout(layout)
        .setBackgroundColor(ContextCompat.getColor(context, android.R.color.white))
        .setLocationByAttachedView(anchorView)
        .setGravity(EasyDialog.GRAVITY_TOP)
        .setTouchOutsideDismiss(true)
        .setMatchParent(false)
        .setOutsideColor(ContextCompat.getColor(context, R.color.easy_out_side_color))
        .setTriangleShow(false)
        .setMarginLeftAndRight(0, 0).showAlert(Utils.isKeyBoardShow);
  }

  public static synchronized void showEasyDialogNoTriagle(final Context context, View layout, int[] locaiton) {
    easyDialog = new EasyDialog(context).setLayout(layout)
        .setBackgroundColor(ContextCompat.getColor(context, android.R.color.white))
        .setLocation(locaiton)
        .setGravity(EasyDialog.GRAVITY_TOP)
        .setTouchOutsideDismiss(true)
        .setMatchParent(false)
        .setOutsideColor(ContextCompat.getColor(context, R.color.easy_out_side_color))
        .setTriangleShow(false)
        .setMarginLeftAndRight(0, 0).showAlert(Utils.isKeyBoardShow);
  }

  private void setCursor(Drawable drawable) {
    if (drawable == null) return;

    try {
      final Field drawableResField = TextView.class.getDeclaredField("mCursorDrawableRes");
      drawableResField.setAccessible(true);

      final Object drawableFieldOwner;
      final Class<?> drawableFieldClass;
      if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
        drawableFieldOwner = this.messageInputEdt;
        drawableFieldClass = TextView.class;
      } else {
        final Field editorField = TextView.class.getDeclaredField("mEditor");
        editorField.setAccessible(true);
        drawableFieldOwner = editorField.get(this.messageInputEdt);
        drawableFieldClass = drawableFieldOwner.getClass();
      }
      final Field drawableField = drawableFieldClass.getDeclaredField("mCursorDrawable");
      drawableField.setAccessible(true);
      drawableField.set(drawableFieldOwner, new Drawable[]{drawable, drawable});
    } catch (Exception ignored) {
      Logger.log(ignored);
    }
  }

  /**
   * Interface definition for a callback to be invoked when user pressed 'submit' button
   */
  public interface InputListener {

    /**
     * Fires when user presses 'send' button.
     *
     * @param input input entered by user
     * @return if input text is valid, you must return {@code true} and input will be cleared, otherwise return false.
     */
    boolean onSubmit(CharSequence input);

    boolean onVoiceClick();
  }

  /**
   * Interface definition for a callback to be invoked when user presses 'add' button
   */
  public interface AttachmentsListener {

    /**
     * Fires when user presses 'add' button.
     */
    void onAddAttachments();
  }

  public String getText() {
    return messageInputEdt.getText().toString();
  }

}
