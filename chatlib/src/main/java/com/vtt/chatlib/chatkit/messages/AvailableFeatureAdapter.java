package com.vtt.chatlib.chatkit.messages;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vtt.chatlib.R;
import com.vtt.chatlib.chatdetail.data.model.AvailableFeature;

import java.util.ArrayList;

/**
 * Created by Hungpq on 1/3/2018.
 */

public class AvailableFeatureAdapter extends RecyclerView.Adapter<AvailableFeatureAdapter.ViewHolder> {

  private ArrayList<AvailableFeature> availableFeatures;
  private OnItemClick onItemClick;
  private int maxHeight = 0;

  public AvailableFeatureAdapter(ArrayList<AvailableFeature> availableFeatures, OnItemClick onItemClick) {
    this.availableFeatures = availableFeatures;
    this.onItemClick = onItemClick;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_available_feature, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    final AvailableFeature availableFeature = availableFeatures.get(position);
    Glide.with(holder.itemView.getContext())
        .load(availableFeature.getIconUrl()).into(holder.imgIcon);

    holder.txtTitle.setText(availableFeature.getTitle());
    holder.txtSubtitle.setText(availableFeature.getSubTitle());

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (onItemClick != null) onItemClick.onItemAvailableFeatureClick(availableFeature);
      }
    });

    holder.layoutContent.post(new Runnable() {
      @Override
      public void run() {
        int viewHeight = holder.layoutContent.getHeight();
        if (viewHeight > maxHeight) {
          maxHeight = viewHeight;
          notifyDataSetChanged();
        } else if (maxHeight > 0) {
          RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.layoutContent.getLayoutParams();
          layoutParams.height = maxHeight;
          holder.layoutContent.setLayoutParams(layoutParams);
        }
      }
    });

  }

  @Override
  public int getItemCount() {
    return availableFeatures.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {

    ImageView imgIcon;
    TextView txtTitle;
    TextView txtSubtitle;
    View layoutContent;

    public ViewHolder(View itemView) {
      super(itemView);

      imgIcon = (ImageView) itemView.findViewById(R.id.img_icon);
      txtTitle = (TextView) itemView.findViewById(R.id.txt_title);
      txtSubtitle = (TextView) itemView.findViewById(R.id.txt_subtitle);
      layoutContent = itemView.findViewById(R.id.content_view);
    }
  }

  public interface OnItemClick {
    void onItemAvailableFeatureClick(AvailableFeature pos);
  }
}
