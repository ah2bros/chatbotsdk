package com.vtt.chatlib.chatkit.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vtt.chatlib.R;
import com.vtt.chatlib.chatkit.models.QuickReply;

import java.util.List;

/**
 * Created by HaiKE on 10/9/17.
 */

public class ActionListAdapter extends RecyclerView.Adapter<ActionHolder> {
  protected List<QuickReply> datas;
  private Context context;
  private MessagesListAdapter.OnMessageActionClickListener onMessageActionClickListener;
  private int messageHolderPosition = -1;

  public ActionListAdapter(Context context, List<QuickReply> datas,
                           MessagesListAdapter.OnMessageActionClickListener onMessageActionClickListener,
                           int messageHolderPosition) {
    super();
    this.context = context;
    this.datas = datas;
    this.onMessageActionClickListener = onMessageActionClickListener;
    this.messageHolderPosition = messageHolderPosition;
  }

  @Override
  public int getItemCount() {
    if (datas != null && datas.size() > 0)
      return datas.size();
    return 0;
  }

  @Override
  public ActionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.item_action, parent, false);
    return new ActionHolder(view, context);
  }

  @Override
  public int getItemViewType(int position) {
    return super.getItemViewType(position);
  }

  @SuppressWarnings("deprecation")
  @Override
  public void onBindViewHolder(ActionHolder holder, final int position) {
    if (holder instanceof ActionHolder) {
      ActionHolder itemHolder = holder;
      itemHolder.bind(datas.get(position));
      itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (onMessageActionClickListener != null)
            onMessageActionClickListener.onMessageQuickReplyClick(datas.get(position), messageHolderPosition);
        }
      });

    }
  }
}
