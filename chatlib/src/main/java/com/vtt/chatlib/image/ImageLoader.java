package com.vtt.chatlib.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.NotificationTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.vtt.chatlib.R;
import com.vtt.chatlib.utils.Logger;

import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Created by HaiKE on 14/07/16.
 */
public class ImageLoader {

  private static final String TAG = ImageLoader.class.getSimpleName();

  public static void setImage(Context context, String url, final ImageView imageView) {
    Glide.with(context)
        .load(url)
        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
        .placeholder(context.getResources().getDrawable(R.color.color_animation))
        .error(context.getResources().getDrawable(R.color.color_animation))
        .dontAnimate()
//                .crossFade(FADE_TIME)//Cirle image ko dung cai nay
        .into(imageView);
  }


  public static void setImage(Context context, String url, final ImageLoaderCallback callback) {
    Glide.with(context).load(url).asBitmap()
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(new SimpleTarget<Bitmap>() {
          @Override
          public void onResourceReady(Bitmap bm, GlideAnimation<? super Bitmap> glideAnimation) {
            callback.onCompleted(bm);
          }

          @Override
          public void onLoadFailed(Exception e, Drawable errorDrawable) {
            callback.onFailed(e);
          }

        });
  }

  public void setImageBlurBuilder(Context context, String url, final ImageView image, final int radius) {
    try {
      Glide.with(context).load(url)
//                    .placeholder(R.drawable.bg_player1)
//                    .error(R.drawable.bg_player1)
          .diskCacheStrategy(DiskCacheStrategy.SOURCE)
          .dontAnimate()
          .bitmapTransform(new BlurTransformation(context, radius))
          .into(image);
    } catch (Exception e) {
      Logger.log(e);
    }
  }

  public static void setNotification(Context context, String url, int placeholder, int error, NotificationTarget target) {
    try {
      Glide.with(context).load(url).asBitmap().placeholder(placeholder).error(error).into(target);
    } catch (Exception e) {
      Logger.log(e);
    }
  }

}
