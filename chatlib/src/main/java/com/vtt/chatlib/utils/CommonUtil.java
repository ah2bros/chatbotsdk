package com.vtt.chatlib.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bavv on 5/23/17.
 */

public class CommonUtil {

  private static final int MAX_LENGTH = 10;

  public static String replaceLastThree(String s) {
    if (null == s) return "";

//    Log.e(CommonUtil.class.getCanonicalName(), "replaceLastThree:: s == " + s);

    int length = s.length();
    if (length <= MAX_LENGTH) return s;

    s = s.substring(0, MAX_LENGTH);
    length = s.length();

    return s.substring(0, length - 3) + "...";
  }

  public static boolean isHasMarshmallow() {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
  }

  public static boolean hasPermissions(Context context, String... permissions) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
      for (String permission : permissions) {
        if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
          return false;
        }
      }
    }
    return true;
  }

  public static float getPercent() {
    return 1;
  }

  public static float getProvincePercent() {
    return 1;
  }

  public static <T> boolean isEqualIgnoreOrder(List<T> l1, List<T> l2) {
    if (l1 == null && l2 == null) return true;
    if (l1 == null || l2 == null) return false;
    if (l1.size() != l2.size()) {
      return false;
    }
    Map<T, Integer> m1 = createMap(l1);
    Map<T, Integer> m2 = createMap(l2);
    return m1.equals(m2);
  }

  protected static <T> Map<T, Integer> createMap(List<T> list) {
    Map<T, Integer> map = new HashMap<T, Integer>();
    for (T item : list) {
      Integer prevCount = map.get(item);
      map.put(item, prevCount == null ? 1 : prevCount + 1);
    }
    return map;
  }
}
