package com.vtt.chatlib;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.vtt.chatlib.chatheads.UltimateChatHeaderService;
import com.vtt.chatlib.network.WSRestful;

import java.util.HashMap;

import retrofit2.Response;

/**
 * Created by phamh_000 on 05/12/2017.
 */

public class ChatBotUtils {

  private ChatBotUtils() {
  }

  private static ChatBotUtils chatBotUtils;

  public static ChatBotUtils getInstance() {
    if (chatBotUtils == null)
      chatBotUtils = new ChatBotUtils();
    return chatBotUtils;
  }

  public static String ENDPOINT_CHATBOT = "http://10.60.129.68:8057/vBIServices/";
  public static String DOMAIN = "mbi";
  public static String ENDPOINT_MBI = "http://10.60.129.68:8057/vBIServices/";
  public static Object data;
  public static HashMap<String, String> headers;

  private static UltimateChatHeaderService chatHeadService;

  private static boolean bound = false;

  private OnChatActionCallback onChatActionCallback;

  /**
   * Method to open/show chat head
   */
  public static void connectChatHead(Context activity, Object data, String domain, HashMap<String, String> headers) {
    if (domain != null && !domain.isEmpty()) {
      DOMAIN = domain;
    }
    ChatBotUtils.data = data;
    ChatBotUtils.headers = headers;
    new WSRestful(activity).resetServiceAll();
    startService(activity);
  }

  public static void destroyChatHead(Context context) {
    context.stopService(new Intent(context, UltimateChatHeaderService.class));
    hideChatHead();
  }

  /**
   * Method to minimize chathead
   */
  public static void minimize() {
    if (chatHeadService != null) chatHeadService.minimize();
  }

  /**
   * Method to maximize chathead
   */
  public static void maximize() {
    if (chatHeadService != null) chatHeadService.maximize();
  }

  /**
   * Method to hide ChatHead
   */
  public static void hideChatHead() {
    if (chatHeadService != null) chatHeadService.removeAllChatHeads();
  }

  /**
   * Method to add loading message in bottom list
   */
  public static void addLoadingMessage() {
    if (chatHeadService != null) chatHeadService.showLoading();
  }

  public static void showChatHead() {
    if (chatHeadService != null) chatHeadService.addChatHead();
  }

  private static void startService(Context activity) {
    Intent intent = new Intent(activity, UltimateChatHeaderService.class);
    activity.startService(intent);
    activity.bindService(intent, new ServiceConnection() {
      @Override
      public void onServiceConnected(ComponentName name, IBinder service) {
        UltimateChatHeaderService.LocalBinder binder = (UltimateChatHeaderService.LocalBinder) service;
        chatHeadService = binder.getService();
        bound = true;
        chatHeadService.maximize();

        chatHeadService.addChatHead();
      }

      @Override
      public void onServiceDisconnected(ComponentName name) {
        chatHeadService = null;
        bound = false;
      }
    }, Context.BIND_AUTO_CREATE);

  }

  public OnChatActionCallback getOnChatActionCallback() {
    return onChatActionCallback;
  }

  public void setOnChatActionCallback(OnChatActionCallback onChatActionCallback) {
    this.onChatActionCallback = onChatActionCallback;
  }

  public interface OnChatActionCallback {
    void onAction(String json, OnChatBotActionDone onChatBotActionDone);

    void onVoiceRequest(OnChatBotActionDone onChatBotActionDone);

    void onErrorMessage(Response response);
  }

  public interface OnChatBotActionDone {
    void onActionDone(String json);

    void onVoiceRecognitionResult(String firstMatch);
  }
}
