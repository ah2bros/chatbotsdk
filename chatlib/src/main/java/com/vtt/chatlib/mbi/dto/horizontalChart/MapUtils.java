package com.vtt.chatlib.mbi.dto.horizontalChart;

import com.vtt.chatlib.mbi.dto.common.ItemLegend;
import com.vtt.chatlib.mbi.dto.common.LegendType;
import com.vtt.chatlib.mbi.dto.lineChart.GroupColumnPieceStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hungpq on 12/14/2017.
 */

public class MapUtils {
  public static List<ItemLegend> toListItemLegends(List<GroupColumnPieceStyle> styles) {
    List<ItemLegend> itemLegends = new ArrayList<>();
    for (GroupColumnPieceStyle style : styles) {
      ItemLegend i = new ItemLegend(style);
      i.setLegendType(LegendType.BARCHART);
      itemLegends.add(i);
    }
    return itemLegends;
  }
}
