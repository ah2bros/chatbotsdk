package com.vtt.chatlib.mbi.dto.common;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;

/**
 * Created by Administrator on 4/8/2017.
 */

public class LineLongClickListener implements OnChartGestureListener, View.OnTouchListener {
  private static final long LONG_CLICK_DURATION = 200;
  LongPressListener listener;

  public LineLongClickListener setListener(LongPressListener listener) {
    this.listener = listener;
    return this;
  }

  public interface LongPressListener {
    void onLongPress(MotionEvent me);

    void onEndLongPress(MotionEvent me);

    boolean onLongPressMove(MotionEvent me);
  }

  @Override
  public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

  }

  @Override
  public void onChartGestureEnd(final MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
    listener.onEndLongPress(me);
  }

  @Override
  public void onChartLongPressed(MotionEvent me) {
//    if (listener != null) {
//      listener.onLongPress(me);
//    }
  }

  @Override
  public void onChartDoubleTapped(MotionEvent me) {
  }

  @Override
  public void onChartSingleTapped(MotionEvent me) {
  }

  @Override
  public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
  }

  @Override
  public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
  }

  @Override
  public void onChartTranslate(MotionEvent me, float dX, float dY) {
  }

  private Handler handler = new Handler();

  @Override
  public boolean onTouch(View v, final MotionEvent event) {
    if (event.getAction() == MotionEvent.ACTION_DOWN) {
      handler.postDelayed(new Runnable() {
        @Override
        public void run() {
          listener.onLongPress(event);
        }
      }, LONG_CLICK_DURATION);
      return false;
    }
    if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
      listener.onEndLongPress(event);
      handler.removeCallbacksAndMessages(null);
      return false;
    }
    if (event.getAction() == MotionEvent.ACTION_MOVE) {
      return listener.onLongPressMove(event);
    }
    return false;
  }
}
