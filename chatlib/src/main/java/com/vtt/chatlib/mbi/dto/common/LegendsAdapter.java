package com.vtt.chatlib.mbi.dto.common;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.vtt.chatlib.R;
import com.vtt.chatlib.utils.Utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 4/7/2017.
 */

public class LegendsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  List<ItemLegend> itemLegends;
  boolean isShowValue;
  boolean isShowPercent;
  boolean multiLineName;

  int oneRowHeight;
  private OnItemLegendClickListener onItemLegendClickListener;

  private List<ItemLegend> itemsTitleHiden = new ArrayList<>();

//  public LegendsAdapter(List<ItemLegend> itemLegends) {
//    this.itemLegends = itemLegends;
//  }

  public LegendsAdapter(List<ItemLegend> itemLegends, boolean multiLineName) {
    this.itemLegends = itemLegends;
    this.multiLineName = multiLineName;
  }

  public class LegendViewHolder extends RecyclerView.ViewHolder {
    RelativeLayout mIndicator;
    TextView mTitleTv;
    TextView mValueTv;
    TextView mPercentTv;

    public LegendViewHolder(View itemView) {
      super(itemView);
      mIndicator = (RelativeLayout) itemView.findViewById(R.id.item_legend_indicator);
      mTitleTv = (TextView) itemView.findViewById(R.id.item_legend_title);
      mValueTv = (TextView) itemView.findViewById(R.id.item_legend_value_tv);
      mPercentTv = (TextView) itemView.findViewById(R.id.item_legend_percent_tv);

    }
  }

  public class GroupHolder extends RecyclerView.ViewHolder {
    TextView mNameTv;
    TextView mUnitTv;

    public GroupHolder(View itemView) {
      super(itemView);
      mNameTv = (TextView) itemView.findViewById(R.id.item_pop_over_group_name_tv);
      mUnitTv = (TextView) itemView.findViewById(R.id.item_pop_over_group_unit_tv);
    }
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    switch (viewType) {
      case -1:
        return new GroupHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_popover_group_chat, parent, false));
      case 1:
        return new LegendViewHolder(LayoutInflater
            .from(parent.getContext())
            .inflate(R.layout.item_legend_detail_chart_chat, parent, false));
      default:
        return new LegendViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_legend_chat, parent, false));
    }
  }

  @Override
  public void onBindViewHolder(final RecyclerView.ViewHolder h, int position) {
    final ItemLegend itemLegend = itemLegends.get(position);
    if (null == itemLegend) return;

    if (itemLegend.getLegendType() != null &&
        itemLegend.getLegendType().equals(LegendType.GROUP_NAME)) {
      GroupHolder holder = (GroupHolder) h;
      holder.mNameTv.setText(itemLegend.getmTitle());
      holder.mUnitTv.setText(itemLegend.getDesc());
      return;
    }

    if (itemLegend.getTitle() != null)
      if (itemsTitleHiden.contains(itemLegend)) {
        setViewGone(h.itemView);
      } else {
        setViewVisible(h.itemView);
      }

    LegendViewHolder holder = (LegendViewHolder) h;
    holder.mTitleTv.setText(itemLegend.getTitle());
    holder.mIndicator.removeAllViews();
    if (itemLegend.getLegendType() == null) {
      holder.mIndicator.setBackgroundDrawable(Utils.getGradientDrawable(itemLegend.getColor()));
    } else {
      switch (itemLegend.getLegendType()) {
        case LINECHART:
          LineChartIndicator indicator = new LineChartIndicator(holder.itemView.getContext());
          indicator.setColor(itemLegend.getColor(), holder.itemView.getContext());
          holder.mIndicator.addView(indicator);
          break;
        case BARCHART:
          holder.mIndicator.addView(generatSimpleIndicator(holder.itemView.getContext(), itemLegend.getColor(), false));
          break;
        case PIE_CHART:
          holder.mIndicator.addView(generatSimpleIndicator(holder.itemView.getContext(), itemLegend.getColor(), true));
          break;
        case DASHED_LINE:
          LineChartIndicator dashed = new LineChartIndicator(holder.itemView.getContext());
//          dashed.setColor(itemLegend.getColor());
          dashed.setDashed(itemLegend.getColor(), holder.itemView.getContext());
          holder.mIndicator.addView(dashed);
          break;
        case DASHED_LINE_NO_INDICATOR:
        case LINE_NO_INDICATOR:
          LineChartIndicator dashedNoIndicator = new LineChartIndicator(holder.itemView.getContext());
          dashedNoIndicator.setColor(itemLegend.getColor(), holder.itemView.getContext());
          dashedNoIndicator.setDashed(itemLegend.getColor(), holder.itemView.getContext());
          dashedNoIndicator.setmIndicatorViewColor(0, holder.itemView.getContext());
          holder.mIndicator.addView(dashedNoIndicator);
          break;
//        case LINE_NO_INDICATOR:
//          LineChartIndicator lineNoIndicator = new LineChartIndicator(holder.itemView.getContext());
//          lineNoIndicator.setColor(itemLegend.getColor());
//          lineNoIndicator.setmIndicatorViewColor(0);
//          holder.mIndicator.addView(lineNoIndicator);
        default:
          holder.mIndicator.setBackgroundDrawable(Utils.getGradientDrawable(itemLegend.getColor()));
          break;
      }

    }
    if (isShowValue) {
      holder.mValueTv.setText(itemLegend.getViewValue());
      holder.mValueTv.setVisibility(View.VISIBLE);
    } else holder.mValueTv.setVisibility(View.GONE);

    if (isShowPercent && StringUtils.isNotEmpty(itemLegend.getPercent())) {
      holder.mPercentTv.setText(" (" + itemLegend.getPercent() + ")");
      holder.mPercentTv.setVisibility(View.VISIBLE);
    } else holder.mPercentTv.setVisibility(View.GONE);

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (onItemLegendClickListener != null)
          onItemLegendClickListener.onItemLegendClicked(itemLegend);
      }
    });
  }

  @Override
  public int getItemViewType(int position) {
    if (itemLegends.get(position).getLegendType() != null
        && itemLegends.get(position).getLegendType().equals(LegendType.GROUP_NAME))
      return -1;
    return multiLineName ? 1 : 0;
//    return itemLegends.get(position).getLegendType().ordinal();
  }

  @Override
  public int getItemCount() {
    return itemLegends.size();
  }

  public LegendsAdapter setShowValue(boolean showValue) {
    isShowValue = showValue;
    return this;
  }

  public LegendsAdapter setShowPercent(boolean showPercent) {
    isShowPercent = showPercent;
    return this;
  }

  public interface OnItemLegendClickListener {
    void onItemLegendClicked(ItemLegend itemLegend);
  }

  public LegendsAdapter setOnItemLegendClickListener(OnItemLegendClickListener onItemLegendClickListener) {
    this.onItemLegendClickListener = onItemLegendClickListener;
    return this;
  }

  public int getOneRowHeight() {
    return oneRowHeight;
  }

  public void reset(List<ItemLegend> itemLegends) {
    this.itemLegends.clear();
    this.itemLegends.addAll(itemLegends);
  }

  private View generatSimpleIndicator(Context context, int color, boolean circle) {
    View v = new View(context);
    int indicatorSize = context.getResources().getDimensionPixelSize(R.dimen.indicator_chart);
    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(indicatorSize, indicatorSize);
    if (!circle)
      v.setBackgroundColor(color);
    else
      v.setBackgroundDrawable(Utils.getGradientDrawable(color));
    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
    v.setLayoutParams(layoutParams);
    return v;
  }

  public void setViewGone(View itemView) {
    ViewGroup.LayoutParams param = itemView.getLayoutParams();
    itemView.setVisibility(View.GONE);
    param.height = 0;
    param.width = 0;
    itemView.setLayoutParams(param);
  }

  public void setViewVisible(View itemView) {
    ViewGroup.LayoutParams param = itemView.getLayoutParams();
    itemView.setVisibility(View.VISIBLE);
    param.height = ViewGroup.LayoutParams.WRAP_CONTENT;
    param.width = ViewGroup.LayoutParams.MATCH_PARENT;
    itemView.setLayoutParams(param);
  }

  public List<ItemLegend> getItemLegends() {
    return itemLegends;
  }

  public List<ItemLegend> getItemsTitleHiden() {
    return itemsTitleHiden;
  }
}
