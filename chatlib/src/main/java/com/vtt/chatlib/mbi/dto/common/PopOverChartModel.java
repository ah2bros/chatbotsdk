package com.vtt.chatlib.mbi.dto.common;

import java.util.List;

/**
 * Created by Administrator on 4/9/2017.
 */

public class PopOverChartModel {
  private String title;
  private String columnValue;

  private List<ItemLegend> itemLegends;
  private boolean hideValue;
  private boolean hidePercent;

  public PopOverChartModel() {
  }

  public PopOverChartModel(String title, String columnValue, List<ItemLegend> itemLegends) {
    this.title = title;
    this.columnValue = columnValue;
    this.itemLegends = itemLegends;
  }

  public String getTitle() {
    return title;
  }

  public PopOverChartModel setTitle(String title) {
    this.title = title;
    return this;
  }

  public String getColumnValue() {
    return columnValue;
  }

  public PopOverChartModel setColumnValue(String columnValue) {
    this.columnValue = columnValue;
    return this;
  }

  public List<ItemLegend> getItemLegends() {
    return itemLegends;
  }

  public PopOverChartModel setItemLegends(List<ItemLegend> itemLegends) {
    this.itemLegends = itemLegends;
    return this;
  }

  public boolean isHideValue() {
    return hideValue;
  }

  public PopOverChartModel setHideValue(boolean hideValue) {
    this.hideValue = hideValue;
    return this;
  }

  public boolean isHidePercent() {
    return hidePercent;
  }

  public PopOverChartModel setHidePercent(boolean hidePercent) {
    this.hidePercent = hidePercent;
    return this;
  }
}
