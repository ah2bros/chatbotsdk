package com.vtt.chatlib.mbi.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hungl on 5/24/2017.
 */

public class Unit implements Serializable {
  @SerializedName("province")
  private String province;
  @SerializedName("district")
  private String district;
  @SerializedName("station")
  private String station;

  public String getProvince() {
    return province;
  }

  public Unit setProvince(String province) {
    this.province = province;
    return this;
  }

  public String getDistrict() {
    return district;
  }

  public Unit setDistrict(String district) {
    this.district = district;
    return this;
  }

  public String getStation() {
    return station;
  }

  public Unit setStation(String station) {
    this.station = station;
    return this;
  }
}
