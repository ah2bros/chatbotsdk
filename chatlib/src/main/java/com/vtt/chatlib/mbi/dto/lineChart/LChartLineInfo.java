package com.vtt.chatlib.mbi.dto.lineChart;

import com.google.gson.annotations.SerializedName;

/**
 * AccDivPlanChartLine DTO
 * Created by NEO on 2/22/2017.
 */
public class LChartLineInfo {
  @SerializedName("key")
  private String key;

  @SerializedName("value")
  private String value;

  public String getKey() {
    return key;
  }

  public LChartLineInfo setKey(String key) {
    this.key = key;
    return this;
  }

  public String getValue() {
    return value;
  }

  public LChartLineInfo setValue(String value) {
    this.value = value;
    return this;
  }
}
