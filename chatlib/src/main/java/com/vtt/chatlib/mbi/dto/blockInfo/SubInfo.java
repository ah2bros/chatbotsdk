package com.vtt.chatlib.mbi.dto.blockInfo;

/**
 * Created by hungl on 5/24/2017.
 */

public class SubInfo {
  private String title;
  private String performValue;
  private String planValue;
  private Float performPercent;
  private String performColor;
  private String deltaValue;
  private String deltaPercent;
  private Boolean increase;
  private String deltaColor;

  public String getTitle() {
    return title;
  }

  public SubInfo setTitle(String title) {
    this.title = title;
    return this;
  }

  public String getPerformValue() {
    return performValue;
  }

  public SubInfo setPerformValue(String performValue) {
    this.performValue = performValue;
    return this;
  }

  public String getPlanValue() {
    return planValue;
  }

  public SubInfo setPlanValue(String planValue) {
    this.planValue = planValue;
    return this;
  }

  public Float getPerformPercent() {
    return performPercent;
  }

  public SubInfo setPerformPercent(Float performPercent) {
    this.performPercent = performPercent;
    return this;
  }

  public String getPerformColor() {
    return performColor == null ? "#000000" : performColor;
  }

  public SubInfo setPerformColor(String performColor) {
    this.performColor = performColor;
    return this;
  }

  public String getDeltaValue() {
    return deltaValue;
  }

  public SubInfo setDeltaValue(String deltaValue) {
    this.deltaValue = deltaValue;
    return this;
  }

  public String getDeltaPercent() {
    return deltaPercent;
  }

  public SubInfo setDeltaPercent(String deltaPercent) {
    this.deltaPercent = deltaPercent;
    return this;
  }

  public Boolean getIncrease() {
    return increase;
  }

  public SubInfo setIncrease(Boolean increase) {
    this.increase = increase;
    return this;
  }

  public String getDeltaColor() {
    return deltaColor == null ? "#000000" : deltaColor;
  }

  public SubInfo setDeltaColor(String deltaColor) {
    this.deltaColor = deltaColor;
    return this;
  }
}
