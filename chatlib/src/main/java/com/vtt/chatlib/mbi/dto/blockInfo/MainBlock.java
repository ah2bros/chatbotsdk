package com.vtt.chatlib.mbi.dto.blockInfo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hungl on 5/24/2017.
 */

public class MainBlock {
  @SerializedName("performValue")
  private String performValue;
  @SerializedName("planValue")
  private String planValue;
  @SerializedName("performPercent")
  private Float performPercent;
  @SerializedName("performColor")
  private String performColor;

  public String getPerformValue() {
    return performValue;
  }

  public MainBlock setPerformValue(String performValue) {
    this.performValue = performValue;
    return this;
  }

  public String getPlanValue() {
    return planValue;
  }

  public MainBlock setPlanValue(String planValue) {
    this.planValue = planValue;
    return this;
  }

  public Float getPerformPercent() {
    return performPercent;
  }

  public MainBlock setPerformPercent(Float performPercent) {
    this.performPercent = performPercent;
    return this;
  }

  public String getPerformColor() {
    return performColor == null ? "#000000" : performColor;
  }

  public MainBlock setPerformColor(String performColor) {
    this.performColor = performColor;
    return this;
  }
}
