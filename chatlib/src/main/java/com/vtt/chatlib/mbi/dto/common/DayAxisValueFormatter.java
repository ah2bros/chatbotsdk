package com.vtt.chatlib.mbi.dto.common;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.vtt.chatlib.utils.CommonUtil;

import java.util.List;

/**
 * Day AxisValue Formatter
 * Created by NEO on 2/22/2017.
 */

public class DayAxisValueFormatter implements IAxisValueFormatter {
  private BarLineChartBase<?> mChart;
  private List<String> mDates;

  public DayAxisValueFormatter(BarLineChartBase<?> chart, List<String> dates) {
    mChart = chart;
    mDates = dates;
  }

  @Override
  public String getFormattedValue(float value, AxisBase axis) {

    int days = (int) value;
    String day = days < 0 || days >= mDates.size() ? "" : mDates.get(days);

    return CommonUtil.replaceLastThree(day);
  }
}

/**
 * @Override public String getFormattedValue(float value, AxisBase axis) {
 * <p>
 * int days = (int) value;
 * <p>
 * int year = determineYear(days);
 * <p>
 * int month = determineMonth(days);
 * String monthName = mMonths[month % mMonths.length];
 * String yearName = String.valueOf(year);
 * <p>
 * if (chart.getVisibleXRange() > 30 * 6) {
 * <p>
 * return monthName + " " + yearName;
 * } else {
 * <p>
 * int dayOfMonth = determineDayOfMonth(days, month + 12 * (year - 2016));
 * <p>
 * String appendix = "th";
 * <p>
 * switch (dayOfMonth) {
 * case 1:
 * appendix = "st";
 * break;
 * case 2:
 * appendix = "nd";
 * break;
 * case 3:
 * appendix = "rd";
 * break;
 * case 21:
 * appendix = "st";
 * break;
 * case 22:
 * appendix = "nd";
 * break;
 * case 23:
 * appendix = "rd";
 * break;
 * case 31:
 * appendix = "st";
 * break;
 * }
 * <p>
 * return dayOfMonth == 0 ? "" : dayOfMonth + appendix + " " + monthName;
 * }
 * }
 * <p>
 * private int getDaysForMonth(int month, int year) {
 * <p>
 * // month is 0-based
 * <p>
 * if (month == 1) {
 * boolean is29Feb = false;
 * <p>
 * if (year < 1582)
 * is29Feb = (year < 1 ? year + 1 : year) % 4 == 0;
 * else if (year > 1582)
 * is29Feb = year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
 * <p>
 * return is29Feb ? 29 : 28;
 * }
 * <p>
 * if (month == 3 || month == 5 || month == 8 || month == 10)
 * return 30;
 * else
 * return 31;
 * }
 * <p>
 * private int determineMonth(int dayOfYear) {
 * <p>
 * int month = -1;
 * int days = 0;
 * <p>
 * while (days < dayOfYear) {
 * month = month + 1;
 * <p>
 * if (month >= 12)
 * month = 0;
 * <p>
 * int year = determineYear(days);
 * days += getDaysForMonth(month, year);
 * }
 * <p>
 * return Math.max(month, 0);
 * }
 * <p>
 * private int determineDayOfMonth(int days, int month) {
 * <p>
 * int count = 0;
 * int daysForMonths = 0;
 * <p>
 * while (count < month) {
 * <p>
 * int year = determineYear(daysForMonths);
 * daysForMonths += getDaysForMonth(count % 12, year);
 * count++;
 * }
 * <p>
 * return days - daysForMonths;
 * }
 * <p>
 * private int determineYear(int days) {
 * <p>
 * if (days <= 366)
 * return 2016;
 * else if (days <= 730)
 * return 2017;
 * else if (days <= 1094)
 * return 2018;
 * else if (days <= 1458)
 * return 2019;
 * else
 * return 2020;
 * <p>
 * }
 * }
 */
