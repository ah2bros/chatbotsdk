package com.vtt.chatlib.mbi.dto.common;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.vtt.chatlib.R;
import com.vtt.chatlib.utils.Utils;


/**
 * Created by HoaPham on 5/18/17.
 */

public class LineChartIndicator extends RelativeLayout {
  View mColorView;
  View mIndicatorView;
  DashedLineView mDashedView;

  public LineChartIndicator(Context context) {
    super(context);
    init(context, null);
  }

  public LineChartIndicator(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs);
  }

  public LineChartIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs);
  }

  private void init(Context context, AttributeSet attrs) {
    inflate(getContext(), R.layout.legend_line_indicator_chat, this);
    mColorView = findViewById(R.id.legend_line_color);
    mIndicatorView = findViewById(R.id.legend_line_indicator);
    mDashedView = (DashedLineView) findViewById(R.id.dashed_line_color);
    setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
  }

  public void setLineColor(int color) {
    mColorView.setBackgroundColor(color);
  }

  public void setmIndicatorViewColor(int color, Context context) {
    if (color == 0)
      mIndicatorView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
    else
      mIndicatorView.setBackgroundDrawable(Utils.getGradientDrawable(color));
  }

  public void setColor(int color, Context context) {
    setLineColor(color);
    setmIndicatorViewColor(color, context);
    mDashedView.setVisibility(GONE);
  }

  public void setDashed(int color, Context context) {
    setmIndicatorViewColor(color, context);
    mColorView.setVisibility(GONE);
    mDashedView.setVisibility(VISIBLE);
    mDashedView.setColor(color);
  }
}
