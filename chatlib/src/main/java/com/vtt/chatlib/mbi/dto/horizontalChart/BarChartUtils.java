package com.vtt.chatlib.mbi.dto.horizontalChart;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.MPPointD;
import com.vtt.chatlib.mbi.dto.common.Charts;
import com.vtt.chatlib.mbi.dto.common.LegendsAdapter;
import com.vtt.chatlib.mbi.dto.lineChart.GroupColumnPieceStyle;
import com.vtt.chatlib.mbi.uicomponent.ChartPopOver;
import com.vtt.chatlib.utils.CommonUtil;
import com.vtt.chatlib.utils.NumberUtils;
import com.vtt.chatlib.utils.RecyclerUtils;
import com.vtt.chatlib.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hungpq on 12/14/2017.
 */

public class BarChartUtils {


  public static void bindServiceDataHorizontalStackColumnChart(ViewType viewType,
                                                               HorizontalBarChart mBarChart,
                                                               float minX, float maxX,
                                                               ItemHorizontalStackRankingUnitChartData data,
                                                               RecyclerView mLegendRv,
                                                               LegendsAdapter.OnItemLegendClickListener itemLegendClickListener) {

    Charts.initHorizontalTopchart(mBarChart.getContext(), mBarChart, minX, maxX);

    final HashMap<Float, String> mXBottomlabels = new HashMap<>();
    BarDataSet bottomDataSet = getDataSetFromGroupColumnChart(data, mXBottomlabels);
    bottomDataSet.setAxisDependency(YAxis.AxisDependency.RIGHT);

    mBarChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
      @Override
      public String getFormattedValue(float value, AxisBase axis) {
        return CommonUtil.replaceLastThree(mXBottomlabels.get(value) == null ? "" : mXBottomlabels.get(value));
      }
    });

    if (data.isViewMore()) {
    }
    mBarChart.getAxisRight().setAxisMinimum(caculateMinTopChart(data));
    mBarChart.getAxisRight().setAxisMaximum(caculateMaxTopChart(data));
    mBarChart.getXAxis().setAxisMaximum(data.getColumns().size());

    mBarChart.setData(new BarData(bottomDataSet));

    RecyclerUtils.setupGridRecyclerView(mBarChart.getContext(), mLegendRv, 2);

    Charts.setupLegends(mLegendRv, MapUtils.toListItemLegends(data.getColumnPieceStyles()), itemLegendClickListener);

    mBarChart.invalidate();
  }

  static float caculateMinTopChart(ItemHorizontalStackRankingUnitChartData data) {
    float step1 = 0.5f;
    float step2 = 1f;
    float step3 = 11f / 4;

    float topMax = data.getMaxPercent();
    float topMin = data.getMinPercent();
    if (NumberUtils.isZero(topMax) && NumberUtils.isZero(topMin)) {
      return 0;
    }
    //top chart:
    if (topMin > 0 || NumberUtils.isZero(topMin)) {
      return 0;
    }
    if (topMax < 0 || NumberUtils.isZero(topMax)) {
      return round10(topMin);
    }
    // have both negative and positive
    float tiso = Math.abs(topMin / topMax);
    if (tiso < 0.2) {
      return -round10(topMax) / 4;
    }
    if (tiso < step1) {
      return round10(topMin);
    }
    if (tiso < step2) {
      return round10(topMin);
    }
    if (tiso < step3) {
      return 0 - Math.abs(round10(topMax)) * 3 / 2;
    }
    if (tiso > 5) {
      return round10(topMin);
    }

    return 0 - Math.abs(round10(topMax)) * 4;
  }

  static float caculateMaxTopChart(ItemHorizontalStackRankingUnitChartData data) {
    float step1 = 0.5f;
    float step2 = 1f;
    float step3 = 11f / 4;

    float topMax = data.getMaxPercent();
    float topMin = data.getMinPercent();
    if (NumberUtils.isZero(topMax) && NumberUtils.isZero(topMin)) {
      return 100;
    }
    //top chart:
    if (topMin > 0 || NumberUtils.isZero(topMin)) {
      return round10(topMax);
    }
    if (topMax < 0 || NumberUtils.isZero(topMax)) {
      return 0;
    }
    // have both negative and positive
    float tiso = Math.abs(topMin / topMax);
    if (tiso < 0.2) {
      return round10(topMax);
    }
    if (tiso < step1) {
      return Math.abs(round10(topMin)) * 4;
    }
    if (tiso < step2) {
      return Math.abs(round10(topMin)) * 3 / 2;
    }
    if (tiso < step3) {
      return round10(topMax);
    }
    if (tiso > 5) {
      return -round10(topMin) / 4;
    }
    return round10(topMax);
  }

  static int round10(float x) {
    if (NumberUtils.isZero(x)) return 0;
    if (x > 0) {
      return (int) (x + 10 - x % 10);
    } else {
      float y = 0 - x;
      return 0 - round10(y);
    }
  }

  public static BarDataSet getDataSetFromGroupColumnChart(ItemHorizontalStackRankingUnitChartData rankingUnitChartData,
                                                          Map<Float, String> xlabels) {
    if (null == rankingUnitChartData) return null;

    xlabels.clear();
    HashMap<Integer, GroupColumnPieceStyle> styleHashMap = rankingUnitChartData.getMapStylePeice();
    List<GroupColumnPieceStyle> styles = new ArrayList<>();
    List<BarEntry> barEntries = new ArrayList<>();
    List<GroupColumn> columns = rankingUnitChartData.getColumns();
    boolean hasStyle = false;
    for (int i = 0; i < columns.size(); i++) {
      GroupColumn groupColumn = columns.get(i);

      if (null == groupColumn) continue;

      ArrayList<DataColumnPiece> dataColumnPieces = groupColumn.getDataColumnPieces();
      if (null == dataColumnPieces || dataColumnPieces.isEmpty()) {
        barEntries.add(new BarEntry(i, 0));
        xlabels.put((float) (i), groupColumn.getLabel());
        continue;
      }

      float[] values = new float[dataColumnPieces.size()];
      int size = dataColumnPieces.size();
      for (int j = 0; j < size; j++) {
//        if (dataColumnPieces.get(j).getPercent() == null)
//          values[j] = new Float(0);
//        else
        values[j] = dataColumnPieces.get(j).getPercent();
        if (!hasStyle) {
          styles.add(styleHashMap.get(dataColumnPieces.get(j).getStyleId()));
        }
      }
      hasStyle = true;
      barEntries.add(new BarEntry(i, values));
      xlabels.put((float) (i), groupColumn.getLabel());
    }
    BarDataSet dataSet = new BarDataSet(barEntries, rankingUnitChartData.getChartName());
    List<Integer> dataSetColors = new ArrayList<>();
    for (int m = 0; m < styles.size(); m++) {
      String color = styles.get(m).getColor();
      dataSetColors.add(null == color ? Color.BLACK : Color.parseColor(color));
    }
    dataSet.setColors(dataSetColors);
    dataSet.setDrawValues(false);
    dataSet.setHighLightAlpha(0);
    return dataSet;
  }

  public static void setupActionHoldHorizontalColumnChart(final HorizontalBarChart mBarChart,
                                                          final RelativeLayout mContainerView,
                                                          final ItemHorizontalStackRankingUnitChartData data,
                                                          final boolean group) {
    final LinearLayout mGroupChartCoverll = new LinearLayout(mContainerView.getContext());
    mContainerView.addView(mGroupChartCoverll);
    mGroupChartCoverll.setVisibility(View.GONE);

    mBarChart.setOnChartGestureListener(new GuestLongClickListener()
        .setListener(new GuestLongClickListener.LongPressListener() {
          @Override
          public void onLongPress(MotionEvent me) {
            Entry entry = mBarChart.getEntryByTouchPoint(me.getX(), me.getY());
            BarChartUtils.getCoverHorizontalBarChart(mBarChart, entry, mGroupChartCoverll);
            mGroupChartCoverll.setVisibility(View.VISIBLE);
            final String title = data.getChartName() + ((StringUtils.isEmpty(data.getUnit())) ? "" : " (" + data.getUnit() + ")");

            final int entryIndex = mBarChart
                .getBarData()
                .getDataSetByIndex(0)
                .getEntryIndex((BarEntry) mBarChart.getEntryByTouchPoint(me.getX(), me.getY()));
            if (data.getColumns() != null &&
                data.getColumns().size() > 0) {
              mGroupChartCoverll.post(new Runnable() {
                @Override
                public void run() {
                  ChartPopOver.showHorizontalStackRankingUnitDialog(mContainerView.getContext(), mGroupChartCoverll,
                      title, data, entryIndex);
                }
              });

            }
          }

          @Override
          public boolean onLongPressMove(MotionEvent me) {
            return false;
          }

          @Override
          public void onEndLongPress(MotionEvent me) {
            mGroupChartCoverll.setVisibility(View.GONE);
            ChartPopOver.dissmiss();
          }
        }));
  }

  public static void getCoverHorizontalBarChart(HorizontalBarChart mBarchart, Entry entry, LinearLayout mCoverll) {
    LinearLayout cover = getCoverHorizontalBarChart(mBarchart, entry);
    if (NumberUtils.isZero(mCoverll.getX()))
      mCoverll.setX(cover.getX());
    mCoverll.setY(cover.getY());
    mCoverll.setLayoutParams(new RelativeLayout.LayoutParams(cover.getLayoutParams().width, cover.getLayoutParams().height));
    mCoverll.setBackgroundColor(Color.parseColor("#25000000"));
  }

  public static LinearLayout getCoverHorizontalBarChart(HorizontalBarChart mBarChart, Entry entry) {
    LinearLayout mCoverLayer;
    int barWidth = getGroupHorizontalWidth(mBarChart);
    mCoverLayer = new LinearLayout(mBarChart.getContext());
    float y = mBarChart.getPosition(entry, mBarChart.getAxisLeft().
        getAxisDependency()).getY() - barWidth / 2;
    float x;
    x = mBarChart.getPosition(new Entry(0, mBarChart.getAxisRight().getAxisMinimum())
        , mBarChart.getAxisRight().
            getAxisDependency()).getX();

    int coverHight = getBarHeightInPixel(mBarChart);
    mCoverLayer.setY(y);
    mCoverLayer.setX(x);
    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(coverHight, barWidth);
    mCoverLayer.setLayoutParams(params);
    mCoverLayer.setBackgroundColor(Color.parseColor("#25000000"));
    return mCoverLayer;
  }

  public static int getGroupHorizontalWidth(HorizontalBarChart mBarChart) {
    MPPointD point1 = mBarChart.getTransformer(YAxis.AxisDependency.LEFT).getPixelForValues(0, 0);
    MPPointD point2 = mBarChart.getTransformer(YAxis.AxisDependency.LEFT).getPixelForValues(0, 1);
    return (int) (point1.y - point2.y);
  }

  public static int getBarHeightInPixel(BarChart mBarchart) {
    MPPointD point = mBarchart.getTransformer(YAxis.AxisDependency.LEFT)
        .getPixelForValues(mBarchart.getXAxis().getAxisMinimum(), mBarchart.getAxisLeft().getAxisMinimum());
    return (int) point.y;
  }
}
