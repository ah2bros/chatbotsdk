package com.vtt.chatlib.mbi.dto.horizontalChart;

/**
 * Created by HoaPham on 5/7/17.
 */

public enum ViewType {
  VALUE, PERCENT
}
