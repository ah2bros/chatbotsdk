package com.vtt.chatlib.mbi.dto.lineChart;

/**
 * Created by Thanhnc221 on 3/31/2017.
 */
public class GroupColumnPieceStyle {
  private Integer id;
  private String color;
  private String title;
  private Integer objectId;

  public GroupColumnPieceStyle() {
  }

  public GroupColumnPieceStyle(Integer id, String color, String title, Integer objectId) {
    this.id = id;
    this.color = color;
    this.title = title;
    this.objectId = objectId;
  }

  public Integer getObjectId() {
    return objectId;
  }

  public void setObjectId(Integer objectId) {
    this.objectId = objectId;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
