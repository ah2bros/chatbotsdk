package com.vtt.chatlib.mbi.dto.blockInfo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hungl on 5/24/2017.
 */

public class SubBlock {
  @SerializedName("type")
  private String type;
  @SerializedName("data")
  private SubInfo data;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public SubInfo getData() {
    return data;
  }

  public SubBlock setData(SubInfo data) {
    this.data = data;
    return this;
  }
}
