package com.vtt.chatlib.mbi.dto.lineChart;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * AccDivPlanChartLinePoint DTO
 * Created by NEO on 2/22/2017.
 */
public class LChartLinePoint implements Serializable {
  @SerializedName("value")
  private String value;

  @SerializedName("date")
  private String date;
  @SerializedName("viewValue")
  private String viewValue;

  @SerializedName("event")
  private String event;

  public String getValue() {
    return value;
  }

  public LChartLinePoint setValue(String value) {
    this.value = value;
    return this;
  }

  public String getDate() {
    return date;
  }

  public LChartLinePoint setDate(String date) {
    this.date = date;
    return this;
  }

  public String getViewValue() {
    return viewValue;
  }

  public LChartLinePoint setViewValue(String viewValue) {
    this.viewValue = viewValue;
    return this;
  }

  public String getEvent() {
    return event;
  }

  public String getEventText() {
    return (event == null || event.isEmpty()) ? "" : (" - " + event);
  }

  public LChartLinePoint setEvent(String event) {
    this.event = event;
    return this;
  }
}
