package com.vtt.chatlib.mbi.constant;

import com.vtt.chatlib.mbi.dto.BaseItemService;

/**
 * Created by phamh_000 on 11/12/2017.
 */

public class MBIUtils {
  public static int getItemViewType(BaseItemService baseItemService) {
    return ServiceType.valueOf(baseItemService.getType()).ordinal();
  }
}
